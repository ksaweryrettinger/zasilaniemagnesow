#include "cModbusRTU.h"

/**************************************** Read Current Thread **********************************************/

cThreadReadPSU::cThreadReadPSU(cModbus* mbMaster) : wxThread(wxTHREAD_DETACHED)
{
	//Initialize variables
	TimerInitPSU = new cTimerHighRes();
	TimerReadCurrent = new cTimerHighRes();
	this->mbMaster = mbMaster;
    eActiveTract = None;
    mkNum = 0;
    zmkZap = 0;
    ZMKConnected = 0;
    rdBuffer = 0;
	rdStatus = false;
    rc = -1;
    bInitSuccess = false;
    bTractSwitching = false;

	//Create new RTU configuration
    mbMaster->ctxRTU = modbus_new_rtu(mbMaster->mbPort.mb_str(), MB_BAUD, 'E', 8, 1);
	modbus_set_slave(mbMaster->ctxRTU, mbMaster->mbAddress);
	modbus_rtu_set_serial_mode(mbMaster->ctxRTU, MODBUS_RTU_RS232);
	modbus_set_response_timeout(mbMaster->ctxRTU, MB_RESPONSE_TIMEOUT_SEC, MB_RESPONSE_TIMEOUT_US);
	ct = modbus_connect(mbMaster->ctxRTU);
	bModbusError = (bool) (ct == -1);

	/************************************ Local Data *******************************************/

	{
		wxCriticalSectionLocker local_lock(mbMaster->local_guard);
		mbMaster->bModbusError = bModbusError;
	}

	/************************************ Start Main (Error Mode) ******************************/

	//Modbus connection error
	if (bModbusError)
    {
    	//Signal program to start main window
    	if (CHECK_INIT_ERRORS)
    	{
    		if (!mbMaster->bStartMainError) mbMaster->bStartMainError = true;
    	}
    	else
    	{
    		if (!mbMaster->bStartMain) mbMaster->bStartMain = true;
    	}

    	/************************************ Remote Data ******************************************/

    	{
    		wxCriticalSectionLocker remote_lock(mbMaster->remote_guard);
    		mbMaster->mb_mapping->tab_input_registers[MB_INPUTREG_RTU_ERROR - 1] = (uint16_t) bModbusError;
    	}
    }
}

wxThread::ExitCode cThreadReadPSU::Entry()
{
	if (ct != -1)
	{
		/************************************ PSU Initialization ************************************/

		bInitSuccess = InitialisePSU();
		if (bInitSuccess) mbMaster->StartTCP(); //start TCP/IP thread

		/************************************ Main RTU Loop *****************************************/

		while (!TestDestroy())
		{
			/************************************ Copy data from Main ***********************************/

			if (mbMaster->pMain != NULL)
			{
				wxCriticalSectionLocker main_lock(mbMaster->pMain->main_guard); // @suppress("Field cannot be resolved") @suppress("Invalid arguments")
				eActiveTract = mbMaster->pMain->eActiveTract; // @suppress("Field cannot be resolved")
				bTractSwitching = mbMaster->pMain->bTractSwitching;
			}

			/************************************ Modbus RTU Communication ******************************/

			bModbusError = false;

			{
				wxCriticalSectionLocker mb_lock(mbMaster->mb_guard);
				rc = modbus_read_registers(mbMaster->ctxRTU, 2048, 1, &ZMKConnected);
			}

			if (rc == -1) bModbusError = true;

			if ((!CHECK_INIT_ERRORS || bInitSuccess) && !bTractSwitching)
			{
				if (!bModbusError)
				{
					for (uint8_t i = 0; i < TRACTS[eActiveTract].size(); i++)
					{
						mkNum = TRACTS[eActiveTract][i];
						zmkNum[0] = MK_ZMK[mkNum - 1][0];
						zmkNum[1] = MK_ZMK[mkNum - 1][1];

						for (uint8_t j = 0; j < MK_ZMK_NUM; j++)
						{
							bPSUTimeout[zmkNum[j] - 1] = false;
							bPSUOverrun[zmkNum[j] - 1] = false;

							zmkZap = ZMK_ZAP[zmkNum[j] - 1];

							{
								wxCriticalSectionLocker mb_lock(mbMaster->mb_guard);
								rc = modbus_write_register(mbMaster->ctxRTU, mbMaster->ZAP_Address, zmkZap); //B6 = 0
							}

							if (rc == -1)
							{
								bModbusError = true;
								break;
							}

							zmkZap = ZMK_ZAP[zmkNum[j] - 1] | 0x0040;

							{
								wxCriticalSectionLocker mb_lock(mbMaster->mb_guard);
								rc = modbus_write_register(mbMaster->ctxRTU, mbMaster->ZAP_Address, zmkZap); //B6 = 1
							}

							if (rc == -1)
							{
								bModbusError = true;
								break;
							}

							zmkZap = ZMK_ZAP[zmkNum[j] - 1];

							{
								wxCriticalSectionLocker mb_lock(mbMaster->mb_guard);
								rc = modbus_write_register(mbMaster->ctxRTU, mbMaster->ZAP_Address, zmkZap); //B6 = 0
							}

							if (rc == -1)
							{
								bModbusError = true;
								break;
							}

							TimerReadCurrent->reset();
							wxThread::Sleep(DELAY_CURRENT_READY);

							while (1)
							{
								{
									wxCriticalSectionLocker mb_lock(mbMaster->mb_guard);
									rc = modbus_read_registers(mbMaster->ctxRTU, mbMaster->ODC_Address, 1, &rdBuffer);
								}

								if (rc == -1)
								{
									bModbusError = true;
									break;
								}

								//Negate reading
								rdBuffer = ~rdBuffer;

								//Check reading
								if (!(rdBuffer & 0x4000)) //B14 = 0 (ready)
								{
									if ((rdBuffer & 0x1000) && CHECK_CURRENT_ERRORS) bPSUOverrun[zmkNum[j] - 1] = true; //B12 = 1
									else fCurrent[zmkNum[j] - 1] = (rdBuffer & 0x0FFF) / ADC_1A_READ;
									break;
								}

								if (CHECK_CURRENT_ERRORS)
								{
									//B14 = 1, continue reading
									wxThread::Sleep(DELAY_CURRENT_REPEAT);

									if (TimerReadCurrent->elapsed() > READ_CURRENT_TIMEOUT)
									{
										bPSUTimeout[zmkNum[j] - 1] = true;
										break;
									}
								}
								else
								{
									fCurrent[zmkNum[j] - 1] = 0;
									break;
								}
							}

							UpdateModbusData();

							wxThread::Sleep(DELAY_CURRENT_NEXTUNIT);
						}

						if (bModbusError) break;
					}
				}
			}
			else if ((!bInitSuccess && CHECK_INIT_ERRORS) || bTractSwitching) //READ ALL PSUs
			{
				//Read current from all units
				for (uint8_t i = 0; i < NUM_POWER_UNITS; i++)
				{
					bPSUTimeout[i] = false;
					bPSUOverrun[i] = false;

					zmkZap = ZMK_ZAP[i];

					{
						wxCriticalSectionLocker mb_lock(mbMaster->mb_guard);
						rc = modbus_write_register(mbMaster->ctxRTU, mbMaster->ZAP_Address, zmkZap); //B6 = 0
					}

					if (rc == -1)
					{
						bModbusError = true;
						break;
					}

					zmkZap = ZMK_ZAP[i] | 0x0040;

					{
						wxCriticalSectionLocker mb_lock(mbMaster->mb_guard);
						rc = modbus_write_register(mbMaster->ctxRTU, mbMaster->ZAP_Address, zmkZap); //B6 = 1
					}

					if (rc == -1)
					{
						bModbusError = true;
						break;
					}

					zmkZap = ZMK_ZAP[i];

					{
						wxCriticalSectionLocker mb_lock(mbMaster->mb_guard);
						rc = modbus_write_register(mbMaster->ctxRTU, mbMaster->ZAP_Address, zmkZap); //B6 = 0
					}

					if (rc == -1)
					{
						bModbusError = true;
						break;
					}

					TimerReadCurrent->reset();
					wxThread::Sleep(DELAY_CURRENT_READY);

					while (1)
					{
						{
							wxCriticalSectionLocker mb_lock(mbMaster->mb_guard);
							rc = modbus_read_registers(mbMaster->ctxRTU, mbMaster->ODC_Address, 1, &rdBuffer);
						}

						if (rc == -1)
						{
							bModbusError = true;
							break;
						}

						//Negate reading
						rdBuffer = ~rdBuffer;

						//Check reading
						if (!(rdBuffer & 0x4000)) //B14 = 0 (ready)
						{
							if ((rdBuffer & 0x1000) && CHECK_CURRENT_ERRORS) bPSUOverrun[i] = true; //B12 = 1
							else fCurrent[i] = (rdBuffer & 0x0FFF) / ADC_1A_READ;
							break;
						}

						if (CHECK_CURRENT_ERRORS)
						{
							//B14 = 1, continue reading
							wxThread::Sleep(DELAY_CURRENT_REPEAT);

							if (TimerReadCurrent->elapsed() > READ_CURRENT_TIMEOUT)
							{
								bPSUTimeout[i] = true;
								break;
							}
						}
						else
						{
							fCurrent[i] = 0;
							break;
						}
					}

					UpdateModbusData();

					if (bModbusError) break;
					wxThread::Sleep(DELAY_CURRENT_NEXTUNIT);
				}
			}

			/************************************ Other ************************************************/

			//Signal program to start main window
			if ((!CHECK_INIT_ERRORS || bInitSuccess) && !mbMaster->bStartMain) mbMaster->bStartMain = true;
			else if (!bInitSuccess && !mbMaster->bStartMainError) mbMaster->bStartMainError = true;

			UpdateModbusData();
		}
	}

	return (wxThread::ExitCode) 0;
}

void cThreadReadPSU::UpdateModbusData(void)
{
	/************************************ Local Data *******************************************/

	{
		wxCriticalSectionLocker local_lock(mbMaster->local_guard);

		//Update polarity information
		ZNA[1] = mbMaster->ZNA[1];

		mbMaster->bModbusError = bModbusError;
		mbMaster->bZMKConnected = (bool) ((ZMKConnected & 1) == 0);

		for (uint8_t i = 0; i < NUM_POWER_UNITS; i++)
		{
			mbMaster->bPSUTimeout[i] = bPSUTimeout[i];
			mbMaster->bPSUOverrun[i] = bPSUOverrun[i];
			if (STORE_CURRENT_READINGS) mbMaster->fCurrent[i] = fCurrent[i];
		}
	}

	/************************************ Remote Data ******************************************/

	{
		wxCriticalSectionLocker remote_lock(mbMaster->remote_guard);

		//Modbus RTU Errors
		mbMaster->mb_mapping->tab_input_registers[MB_INPUTREG_RTU_ERROR - 1] = (uint16_t) bModbusError;

		//ZMK Connection Status
		mbMaster->mb_mapping->tab_input_registers[MB_INPUTREG_ZMK_STATUS - 1] = (uint16_t) ((ZMKConnected & 1)== 0);

		//Zero PSU Error Registers
		mbMaster->mb_mapping->tab_input_registers[MB_INPUTREG_PSU_TIMEOUT - 1] = 0;
		mbMaster->mb_mapping->tab_input_registers[MB_INPUTREG_PSU_OVERRUN - 1] = 0;

		for (uint8_t i = 0; i < NUM_POWER_UNITS; i++)
		{
			//Convert reading to string
			strCurrent = wxString::Format(wxT("%.2f"), fCurrent[i]);

			//Store polarity
			if ((ZNA[1] & (1 << (i + 4))) != 0) mbMaster->mb_mapping->tab_input_registers[i] = 0x8000;
			else mbMaster->mb_mapping->tab_input_registers[i] = 0;

			//Store reading in BCD format
			mbMaster->mb_mapping->tab_input_registers[i] |= (strCurrent[0] - '0') << 8;
			mbMaster->mb_mapping->tab_input_registers[i] |= (strCurrent[2] - '0') << 4;
			mbMaster->mb_mapping->tab_input_registers[i] |= (strCurrent[3] - '0');

			//PSU Timeout Errors
			mbMaster->mb_mapping->tab_input_registers[MB_INPUTREG_PSU_TIMEOUT - 1] |= ((uint16_t) bPSUTimeout[i]) << i;

			//PSU Overrun Errors
			mbMaster->mb_mapping->tab_input_registers[MB_INPUTREG_PSU_OVERRUN - 1] |= ((uint16_t) bPSUOverrun[i]) << i;
		}
	}
}

bool cThreadReadPSU::InitialisePSU(void)
{
	bool bSetCurrent = true;
	bool bCurrentOK = false;
	TimerInitPSU->reset();

	while (1)
	{
		//Read current from all units
		for (uint8_t i = 0; i < NUM_POWER_UNITS; i++)
		{
			zmkZap = ZMK_ZAP[i];

			{
				wxCriticalSectionLocker mb_lock(mbMaster->mb_guard);
				rc = modbus_write_register(mbMaster->ctxRTU, mbMaster->ZAP_Address, zmkZap); //B6 = 0
			}


			if (rc == -1)
			{
				bModbusError = true;
				break;
			}

			zmkZap = ZMK_ZAP[i] | 0x0040;

			{
				wxCriticalSectionLocker mb_lock(mbMaster->mb_guard);
				rc = modbus_write_register(mbMaster->ctxRTU, mbMaster->ZAP_Address, zmkZap); //B6 = 1
			}

			if (rc == -1)
			{
				bModbusError = true;
				break;
			}

			zmkZap = ZMK_ZAP[i];

			{
				wxCriticalSectionLocker mb_lock(mbMaster->mb_guard);
				rc = modbus_write_register(mbMaster->ctxRTU, mbMaster->ZAP_Address, zmkZap); //B6 = 0
			}

			if (rc == -1)
			{
				bModbusError = true;
				break;
			}

			TimerReadCurrent->reset();
			wxThread::Sleep(DELAY_CURRENT_READY);

			while (1)
			{
				{
					wxCriticalSectionLocker mb_lock(mbMaster->mb_guard);
					rc = modbus_read_registers(mbMaster->ctxRTU, mbMaster->ODC_Address, 1, &rdBuffer);
				}

				if (rc == -1)
				{
					bModbusError = true;
					break;
				}

				//Negate reading
				rdBuffer = ~rdBuffer;

				//Check reading
				if (!(rdBuffer & 0x4000)) //B14 != 1 (not ready)
				{
					if ((rdBuffer & 0x1000) && CHECK_CURRENT_ERRORS) bPSUOverrun[i] = true; //B12 = 1
					else fCurrent[i] = (rdBuffer & 0x0FFF) / ADC_1A_READ;
					break;
				}

				if (CHECK_CURRENT_ERRORS)
				{
					//B14 = 1, continue reading
					wxThread::Sleep(DELAY_CURRENT_REPEAT);

					if (TimerReadCurrent->elapsed() > READ_CURRENT_TIMEOUT)
					{
						bPSUTimeout[i] = true;
						break;
					}
				}
				else
				{
					fCurrent[i] = 0;
					break;
				}
			}

			UpdateModbusData();

			if (bModbusError) break;
			wxThread::Sleep(DELAY_CURRENT_NEXTUNIT);
		}

		/************************************ Local Data ********************************************/

		{
			wxCriticalSectionLocker local_lock(mbMaster->local_guard);

			mbMaster->bModbusError = bModbusError;

			for (uint8_t i = 0; i < NUM_POWER_UNITS; i++)
			{
				mbMaster->bPSUTimeout[i] = bPSUTimeout[i];
				mbMaster->bPSUOverrun[i] = bPSUOverrun[i];
				if (STORE_CURRENT_READINGS) mbMaster->fCurrent[i] = fCurrent[i];
			}
		}

		/************************************ Zero Current ******************************************/

		if (!bModbusError)
		{
			//Set current to zero for all PSUs
			if (bSetCurrent)
			{
				for (uint8_t i = 0; i < NUM_POWER_UNITS; i++) mbMaster->SetPowerUnitCurrent(i + 1, 0);
				bSetCurrent = false;
			}

			//Reset flag
			bCurrentOK = true;

			//Check current readings
			for (uint8_t i = 0; i < NUM_POWER_UNITS; i++)
			{
				if (bPSUTimeout[i] || bPSUOverrun[i])
				{
					bCurrentOK = false; //read-current errors, unable to check
					break;
				}
				else if (fCurrent[i] >= MIN_SAFE_CURRENT) //current > 0.1A
				{
					bCurrentOK = false;
					break;
				}
			}
		}

		if (bCurrentOK) //initialisation successful
		{
			break;
		}
		else if (TimerInitPSU->elapsed() >= INIT_PSU_TIMEOUT)
		{
			for (uint8_t i = 0; i < NUM_POWER_UNITS; i++)
			{
				if (bPSUTimeout[i] || bPSUOverrun[i] || fCurrent[i] >= MIN_SAFE_CURRENT)
				{
					wxCriticalSectionLocker local_lock(mbMaster->local_guard);
					mbMaster->bInitTimeout[i] = true;
				}
			}

			break; //initialisation timeout
		}
	}

	/************************************ Initialise ZNA Registers ******************************/

	if (bCurrentOK)
	{
		if (!bModbusError)
		{
			//Initialise ZNA registers
			wxCriticalSectionLocker mb_lock(mbMaster->mb_guard);
			rc = modbus_write_register(mbMaster->ctxRTU, mbMaster->ZNA_Address[0], 0);
			rc = modbus_write_register(mbMaster->ctxRTU, mbMaster->ZNA_Address[1], 0);
		}

		return true;
	}
	else return false;
}

cThreadReadPSU::~cThreadReadPSU()
{
	wxCriticalSectionLocker mb_lock(mbMaster->mb_guard);
	modbus_close(mbMaster->ctxRTU);
	modbus_free(mbMaster->ctxRTU);
	mbMaster->mbThreadReadPSU = NULL;
}

/**************************************** Set Active Tract Thread ******************************************/

cThreadSetActiveTract::cThreadSetActiveTract(cModbus *mbMaster, eTract eNewTract) : wxThread(wxTHREAD_DETACHED)
{
    this->mbMaster = mbMaster;
    this->eNewTract = eNewTract;
    mkNum = 0;
    rc = -1;

    {
		wxCriticalSectionLocker local_lock(mbMaster->local_guard);
		bModbusError = mbMaster->bModbusError;
    }

	//Zero ON/OFF bits
	ZNA[0] = 0;
	ZNA[1] = 0; //zero polarity bits as well
}

wxThread::ExitCode cThreadSetActiveTract::Entry()
{
	if (!bModbusError)
	{
		if (eNewTract != None)
		{
			//Update MK-ZMK configuration to new tract
			for (uint8_t i = 0; i < TRACTS[eNewTract].size(); i++)
			{
				mkNum = TRACTS[eNewTract][i];

				if (mkNum > 2) //MK 1-2 always ON
				{
					if (mkNum <= 16 || mkNum >= 20)
					{
						ZNA[0] |= (1 << MK_ZNA[mkNum - 1]);
					}
					else if (mkNum >= 17 && mkNum <= 19)
					{
						ZNA[1] |= (1 << MK_ZNA[mkNum - 1]);
					}
				}
			}
		}

		/******************************* Write MK-ZMK Configuration ********************************/

		{
			wxCriticalSectionLocker mb_lock(mbMaster->mb_guard); //prevent other Modbus RTU operations
			rc = modbus_write_register(mbMaster->ctxRTU, mbMaster->ZNA_Address[0], ZNA[0]);
			rc = modbus_write_register(mbMaster->ctxRTU, mbMaster->ZNA_Address[1], ZNA[1]);
		}

		/************************************ Local Data *******************************************/

		{
			//Save new MK-ZMK configuration
			wxCriticalSectionLocker local_lock(mbMaster->local_guard);
			mbMaster->ZNA[0] = ZNA[0];
			mbMaster->ZNA[1] = ZNA[1];
		}

		/************************************ Main Data ********************************************/

		if (mbMaster->pMain != NULL)
		{
	        wxCriticalSectionLocker main_lock(mbMaster->pMain->main_guard);
	        mbMaster->pMain->eNewTract = eNewTract;
	    }

		/************************************ Remote Data ******************************************/

		{
			wxCriticalSectionLocker remote_lock(mbMaster->remote_guard);
			mbMaster->mb_mapping->tab_input_registers[MB_INPUTREG_ACTIVE_TRACT - 1] = (uint16_t) eNewTract;
		}
	}

	//Terminate thread
	return (wxThread::ExitCode) 0;
}

cThreadSetActiveTract::~cThreadSetActiveTract()
{
	mbMaster->mbThreadSetActiveTract = NULL;
}

/**************************************** Write Current Thread *********************************************/

cThreadWritePSU::cThreadWritePSU(cModbus *mbMaster, uint8_t unitID, float fNewCurrentSetting) : wxThread(wxTHREAD_DETACHED)
{
    this->mbMaster = mbMaster;
    this->fNewCurrentSetting = fabs(fNewCurrentSetting);
    this->unitID = unitID;
    bNegativePolarity = false;
    rc = -1;

    {
		wxCriticalSectionLocker local_lock(mbMaster->local_guard);
		bModbusError = mbMaster->bModbusError;
		fCurrentSetting = fabs(mbMaster->fCurrentSetting[unitID - 1]);
		ZNA[1] = mbMaster->ZNA[1];
    }
}

wxThread::ExitCode cThreadWritePSU::Entry()
{
	if (!bModbusError)
	{
		bNegativePolarity = (ZNA[1] & (1 << (unitID + 3))) != 0;

		if (fabs(fCurrentSetting - fNewCurrentSetting) > H_AMP) //step current change (> 0.50A)
		{
			if (fNewCurrentSetting > fCurrentSetting) //upward step
			{
				while (fCurrentSetting != fNewCurrentSetting && !TestDestroy())
				{
					//Calculate new current step
					if ((fNewCurrentSetting - fCurrentSetting) > H_AMP) fCurrentSetting += H_AMP;
					else fCurrentSetting = fNewCurrentSetting;

					//Write new current step
					{
						wxCriticalSectionLocker mb_lock(mbMaster->mb_guard); //prevent other Modbus RTU operations
						rc = modbus_write_register(mbMaster->ctxRTU, mbMaster->ZMK_Address[unitID - 1], (uint16_t) round(fCurrentSetting * ADC_1A_WRITE));
					}

					//Delay next current step
					wxThread::Sleep(DELAY_CURRENT_STEP);
				}
			}
			else if (fNewCurrentSetting < fCurrentSetting)//downward step
			{
				while (fCurrentSetting != fNewCurrentSetting && !TestDestroy())
				{
					//Calculate new current step
					if ((fCurrentSetting - fNewCurrentSetting) > H_AMP) fCurrentSetting -= H_AMP;
					else fCurrentSetting = fNewCurrentSetting;

					//Write new current step
					{
						wxCriticalSectionLocker mb_lock(mbMaster->mb_guard); //prevent other Modbus RTU operations
						rc = modbus_write_register(mbMaster->ctxRTU, mbMaster->ZMK_Address[unitID - 1], (uint16_t) round(fCurrentSetting * ADC_1A_WRITE));
					}

					//Delay next current step
					wxThread::Sleep(DELAY_CURRENT_STEP);
				}
			}
		}
		else if (!TestDestroy()) //immediate current change
		{
			{
				wxCriticalSectionLocker mb_lock(mbMaster->mb_guard); //prevent other Modbus RTU operations
				rc = modbus_write_register(mbMaster->ctxRTU, mbMaster->ZMK_Address[unitID - 1], (uint16_t) round(fNewCurrentSetting * ADC_1A_WRITE));
			}
		}
	}

	/************************************ Local Data *******************************************/

	{
		wxCriticalSectionLocker local_lock(mbMaster->local_guard);
		if (bNegativePolarity && fCurrentSetting != 0) mbMaster->fCurrentSetting[unitID - 1] = fNewCurrentSetting * (-1);
		else mbMaster->fCurrentSetting[unitID - 1] = fNewCurrentSetting;
		if (!STORE_CURRENT_READINGS) mbMaster->fCurrent[unitID - 1] = fNewCurrentSetting;
	}

	/************************************ Remote Data ******************************************/

	{
		wxCriticalSectionLocker remote_lock(mbMaster->remote_guard);

		//Convert setting to string
		strCurrentSetting = wxString::Format(wxT("%.2f"), fNewCurrentSetting);

		//Store polarity as MSB of BCD current setting
		if (bNegativePolarity && fCurrentSetting != 0) mbMaster->mb_mapping->tab_input_registers[MB_INPUTREG_SETCUR_START + (unitID - 2)] = 0x8000;
		else mbMaster->mb_mapping->tab_input_registers[MB_INPUTREG_SETCUR_START + (unitID - 2)] = 0;

		//Store current setting in BCD format
		mbMaster->mb_mapping->tab_input_registers[MB_INPUTREG_SETCUR_START + (unitID - 2)] |= (strCurrentSetting[0] - '0') << 8;
		mbMaster->mb_mapping->tab_input_registers[MB_INPUTREG_SETCUR_START + (unitID - 2)] |= (strCurrentSetting[2] - '0') << 4;
		mbMaster->mb_mapping->tab_input_registers[MB_INPUTREG_SETCUR_START + (unitID - 2)] |= (strCurrentSetting[3] - '0');
	}

	//Terminate thread
	return (wxThread::ExitCode) 0;
}

cThreadWritePSU::~cThreadWritePSU()
{
	mbMaster->mbThreadWritePSU[unitID - 1] = NULL;
}

/**************************************** Set Polarity Thread **********************************************/

cThreadSetPolarity::cThreadSetPolarity(cModbus *mbMaster, uint8_t unitID, bool bSetNegative) : wxThread(wxTHREAD_DETACHED)
{
    this->mbMaster = mbMaster;
    this->unitID = unitID;
    this->bSetNegative = bSetNegative;
	rc = -1;

	wxCriticalSectionLocker local_lock(mbMaster->local_guard);
	bModbusError = mbMaster->bModbusError;
	ZNA[1] = mbMaster->ZNA[1];
}

wxThread::ExitCode cThreadSetPolarity::Entry()
{
	if (!bModbusError)
	{
		//Update polarity settings
		if (bSetNegative) ZNA[1] |= (1 << (unitID + 3)); //set bit
		else ZNA[1] &= ~(1 << (unitID + 3)); //clear bit

		{
			//Write new polarity settings
			wxCriticalSectionLocker mb_lock(mbMaster->mb_guard); //prevent other Modbus RTU operations
			rc = modbus_write_register(mbMaster->ctxRTU, mbMaster->ZNA_Address[1], ZNA[1]);
		}

		/************************************ Local Data *******************************************/

		{
			wxCriticalSectionLocker local_lock(mbMaster->local_guard); //prevent Local data access

			if (rc != -1)
			{
				if (bSetNegative) mbMaster->ZNA[1] |= (1 << (unitID + 3)); //set bit
				else mbMaster->ZNA[1] &= ~(1 << (unitID + 3)); //clear bit
			}
		}

		/************************************ Remote Data ******************************************/

		{
			wxCriticalSectionLocker remote_lock(mbMaster->remote_guard);

			if (rc != -1)
			{
				if (bSetNegative) mbMaster->mb_mapping->tab_input_registers[MB_INPUTREG_POLARITY - 1] |= (1 << (unitID - 1)); //set bit
				else mbMaster->mb_mapping->tab_input_registers[MB_INPUTREG_POLARITY - 1] &= ~(1 << (unitID - 1)); //clear bit
			}
		}
	}

	//Terminate thread
	return (wxThread::ExitCode) 0;
}

cThreadSetPolarity::~cThreadSetPolarity()
{
	mbMaster->mbThreadSetPolarity[unitID - 1] = NULL;
}
