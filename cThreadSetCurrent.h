#pragma once

#include "ZMKConstants.h"
#include "cMain.h"

class cMain;
class cModbus;
class cTimerHighRes;

/* System clock thread */
class cThreadSetCurrent : public wxThread
{
	public:
		cThreadSetCurrent(cMain*, cModbus*, uint8_t, float);
		~cThreadSetCurrent();

	protected:
		virtual ExitCode Entry();

	private:
		void RefreshModbusData(void);
		bool TestErrors(void);

	private:
		cMain* pMain;
		cModbus* mbMaster;

	private:
		float fCurrent;
		float fCurrentSetting;
		float fNewCurrent;
		bool bPSUTimeout;
		bool bPSUOverrun;
		bool bModbusError;
		bool bTimeout;
		uint16_t ZNA[2] = { 0 };
		int32_t unitID;
		cTimerHighRes* TimerSetCurrent;
};

class cTimerHighRes
{
	public:
		cTimerHighRes() : m_beg(clock_::now()) {}

		//Timer reset
		void reset()
		{
			m_beg = clock_::now();
		}

		//Returns time elapsed in milliseconds since last reset or object creation
		double elapsed() const
		{
			return chrono::duration_cast<chrono::milliseconds>(clock_::now() - m_beg).count();
		}

	private:
		typedef chrono::high_resolution_clock clock_;
		chrono::time_point<clock_> m_beg;
};
