#include "cModbus.h"

cModbus::cModbus(cEntry* pEntry, cMain* pMain, cMainError* pMainError)
{
    this->pMain = pMain;

    //Copy data from entry class
	this->mbAddress = pEntry->mbAddress;
	this->mbPort = pEntry->mbPort;
	this->ZNA_Address[0] = pEntry->ZNA_Address[0];
	this->ZNA_Address[1] = pEntry->ZNA_Address[1];
	this->ODC_Address = pEntry->ODC_Address;
	this->ZAP_Address = pEntry->ZAP_Address;
	for (uint8_t i = 0; i < NUM_POWER_UNITS; i++) this->ZMK_Address[i] = pEntry->ZMK_Address[i];

	//Initialization
    ctxRTU = nullptr;
    bStartMain = false;
    bStartMainError = false;
    bModbusError = false;
    bZMKConnected = false;
    mbThreadTCP = NULL;
    mbThreadSetActiveTract = NULL;

    mb_mapping = modbus_mapping_new_start_address(1, 0, 10001, 0, 40001, 13, 30001, 31);

    //Start main (Modbus RTU) thread
    mbThreadReadPSU = new cThreadReadPSU(this);

    if (mbThreadReadPSU->Run() != wxTHREAD_NO_ERROR)
    {
        wxLogError("Can't create Modbus RTU thread!");
        delete mbThreadReadPSU;
        mbThreadReadPSU = nullptr;
    }

	pMain->mbMaster = this;
	pMainError->mbMaster = this;
}

//Start Modbus TCP thread
void cModbus::StartTCP(void)
{
	if (!mbThreadTCP)
	{
		mbThreadTCP = new cThreadTCP(this);

		if (mbThreadTCP->Run() != wxTHREAD_NO_ERROR)
		{
			wxLogError("Can't create Modbus TCP thread!");
			delete mbThreadTCP;
			mbThreadTCP = nullptr;
	   }
	}
}

//Update MK-ZMK configuration to new tract
void cModbus::SetActiveTract(eTract eNewTract)
{
	//Create new thread for setting new tract
	mbThreadSetActiveTract = new cThreadSetActiveTract(this, eNewTract);

    if (mbThreadSetActiveTract->Run() != wxTHREAD_NO_ERROR)
    {
        wxLogError("Can't create tract-setting thread!");
        delete mbThreadSetActiveTract;
        mbThreadSetActiveTract = NULL;
    }
}

//Set current in single PSU
void cModbus::SetPowerUnitCurrent(uint8_t unitID, float current)
{
	//Create new thread for writing new current value to PSU
	mbThreadWritePSU[unitID - 1] = new cThreadWritePSU(this, unitID, current);

    if (mbThreadWritePSU[unitID - 1]->Run() != wxTHREAD_NO_ERROR)
    {
        wxLogError("Can't create Modbus set-current thread!");
        delete mbThreadWritePSU[unitID - 1];
        mbThreadWritePSU[unitID - 1] = NULL;
    }
}

//Set polarity of single PSU
void cModbus::SetPowerUnitPolarity(uint8_t unitID, bool bSetNegative)
{
	//Create new thread for setting PSU polarity
	mbThreadSetPolarity[unitID - 1] = new cThreadSetPolarity(this, unitID, bSetNegative);

    if (mbThreadSetPolarity[unitID - 1]->Run() != wxTHREAD_NO_ERROR)
    {
        wxLogError("Can't create Modbus pet-polarity thread!");
        delete mbThreadSetPolarity[unitID - 1];
        mbThreadSetPolarity[unitID - 1] = NULL;
    }
}

//Close all connections and terminate active Modbus threads
void cModbus::CloseModbus(void)
{
	//Wait for other RTU threads before terminating main RTU thread
	while (mbThreadSetActiveTract != NULL) wxMilliSleep(1);

	for (uint8_t i = 0; i < NUM_POWER_UNITS; i++)
	{
		while (mbThreadWritePSU[i] != NULL) wxMilliSleep(1);
		while (mbThreadSetPolarity[i] != NULL) wxMilliSleep(1);
	}

	//Delete TCP/IP thread
	if (mbThreadTCP)
	{
		//Delete thread
		if (mbThreadTCP->Delete() != wxTHREAD_NO_ERROR) wxLogError("Can't delete TCP/IP thread!");
		while (mbThreadTCP != NULL) wxMilliSleep(1);
	}

	//Delete RTU thread
	if (mbThreadReadPSU != NULL)
	{
		if (mbThreadReadPSU->Delete() != wxTHREAD_NO_ERROR) wxLogError("Can't delete Main RTU thread!");
	}

	//Wait for RTU thread termination
    while (mbThreadReadPSU != NULL) wxMilliSleep(1);
}
