#include "cMain.h"

wxBEGIN_EVENT_TABLE(cMain, wxFrame)
	EVT_MENU(wxID_INFO, cMain::OnMenuClickedInformation)
	EVT_MENU(wxID_NETWORK, cMain::OnMenuClickedMode)
	EVT_MENU(wxID_CLOSE, cMain::OnMenuClickedExit)
	EVT_COMMAND(wxID_ANY, wxEVT_REMOTE_SET_TRACT, cMain::OnRemoteSetTract)
	EVT_COMMAND(wxID_ANY, wxEVT_REMOTE_SET_CURRENT, cMain::OnRemoteSetCurrent)
	EVT_TIMER(20013, cMain::OnRefreshDisplay)
	EVT_ERASE_BACKGROUND(cMain::OnEraseBackground)
    EVT_CLOSE(cMain::OnClose)
wxEND_EVENT_TABLE()

/* Frame constructor */
cMain::cMain(cEntry* pEntry) : wxFrame(nullptr, wxID_ANY, " ", wxPoint(90, 50), wxSize(1100, 899), (wxDEFAULT_FRAME_STYLE & ~wxRESIZE_BORDER & ~wxMAXIMIZE_BOX & ~wxCLOSE_BOX))
{
    //FRAME TITLE AND COLOUR
    this->SetTitle(_T("Zasilanie Magnesów Korekcyjnych (Sterowanie Zdalne)"));
    this->SetFont(wxFont(11, wxFONTFAMILY_DEFAULT, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_NORMAL));
    this->SetBackgroundColour(wxColour(230, 230, 230));

    //CONSTRUCTOR VARIABLES
    mbMaster = nullptr;
    pInformation = nullptr;
    pPowerCheck = nullptr;
    this->pEntry = pEntry;

    //Other variables initialization
    informationWindowIsOpen = false;

	//MENUS
	mMenuBar = new wxMenuBar();
	mMenuBar->SetFont(wxFont(9, wxFONTFAMILY_DEFAULT, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_NORMAL));
	mMenuBar->SetBackgroundColour(wxColour(221, 220, 220));
	mCloseMenu = new wxMenu();
	mHelpMenu = new wxMenu();
	mModeMenu = new wxMenu();

	if (mHelpMenu != nullptr)
	{
		//Populate drop-down menus
		mHelpMenu->Append(wxID_INFO, _T("Informacje"));
		mModeMenu->Append(wxID_NETWORK, _T("Przejdź na Sterowanie Lokalne"));
		mCloseMenu->Append(wxID_CLOSE, _T("Zakończ"));

		//Append drop-down menus to menu-bar
		mMenuBar->Append(mModeMenu, _T("Sterowanie: Zdalne"));
		mMenuBar->Append(mCloseMenu, _T("Koniec Pracy"));
		mMenuBar->Append(mHelpMenu, _T("O Programie"));
		SetMenuBar(mMenuBar);
	}

	//OTHER VARIABLES
	TimerCloseApp = new cTimerHighRes();
	mkNum = 0;
	zmkIndex = 0;
	bSetTractError = false;
	bTractSwitching = false;
	bModbusError = false;
	bZMKConnected = false;
	eUserMode = Remote;
	eNewTract = None;
	eActiveTract = A;
	bCurrentOK = false;

	//REMOTE CONTROL
	eRemoteNewTract = None;
	newTractNum = 0;
	newCurrentNum = 0;

	//MAIN PANELS
	panTractSelect = new wxPanel(this, wxID_ANY, wxPoint(20, 21), wxSize(501, 120), wxSUNKEN_BORDER);
	panTractDisplay = new wxPanel(this, wxID_ANY, wxPoint(20, 161), wxSize(501, 666), wxSUNKEN_BORDER);
	panControls = new wxPanel(this, wxID_ANY, wxPoint(540, 21), wxSize(540, 806), wxSUNKEN_BORDER);
	panZMKConnected = new wxPanel(this, wxID_ANY, wxPoint(92, 724), wxSize(160, 72), wxNO_BORDER);

	//PANEL TITLES
	txtTitleTractSelect = new wxStaticText(panTractSelect, wxID_ANY, _T("Wybór Traktu"), wxPoint(175, 15), wxSize(150, 20), wxALIGN_CENTRE_HORIZONTAL);
	txtTitleTractSelect->SetFont(wxFont(10, wxFONTFAMILY_DEFAULT, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_BOLD));
	txtTitleControls = new wxStaticText(panControls, wxID_ANY, _T("Brak Wybranego Traktu"), wxPoint(170, 20), wxSize(200, 20), wxALIGN_CENTRE_HORIZONTAL);
	txtTitleControls->SetFont(wxFont(10, wxFONTFAMILY_DEFAULT, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_BOLD));
	txtTitleZMKConnected = new wxStaticText(panZMKConnected, wxID_ANY, _T("Zasilanie\nWłączone"), wxPoint(6, 21), wxSize(100, 50), wxALIGN_CENTRE_HORIZONTAL);
	txtTitleZMKConnected->SetFont(wxFont(10, wxFONTFAMILY_DEFAULT, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_BOLD));
	txtTitleZMKConnected->SetForegroundColour(wxColour(34, 188, 34));

	//TRACT SELECT MODBUS ERROR MESSAGE
	txtModbusError = new wxStaticText(panTractSelect, wxID_ANY, _T("[Błąd Modbus]"), wxPoint(246, 20), wxSize(120, 20), wxALIGN_CENTRE_HORIZONTAL);
	txtModbusError->SetFont(wxFont(10, wxFONTFAMILY_DEFAULT, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_BOLD));
	txtModbusError->SetForegroundColour(wxColour(207, 31, 37));
	txtModbusError->Hide();

	//ZMK CONNECTED MODBUS ERROR MESSAGE
	txtZMKConnectedMBError = new wxStaticText(panZMKConnected, wxID_ANY, _T("[Błąd Modbus]"), wxPoint(3, 27), wxSize(160, 30), wxALIGN_CENTRE_HORIZONTAL);
	txtZMKConnectedMBError->SetFont(wxFont(11, wxFONTFAMILY_DEFAULT, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_BOLD));
	txtZMKConnectedMBError->SetForegroundColour(wxColour(207, 31, 37));
	txtZMKConnectedMBError->Hide();

	//SET-TRACT ERROR MESSAGE
	txtSetTractError = new wxStaticText(panTractSelect, wxID_ANY, _T("[Błąd Zmiany Traktu]"), wxPoint(226, 20), wxSize(160, 20), wxALIGN_CENTRE_HORIZONTAL);
	txtSetTractError->SetFont(wxFont(10, wxFONTFAMILY_DEFAULT, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_BOLD));
	txtSetTractError->SetForegroundColour(wxColour(207, 31, 37));
	txtSetTractError->Hide();

	//IMAGES AND BUTTONS
	for (uint8_t i = 0; i < NUM_TRACTS + 1; i++)
	{
		//Tract Selection Images
		imgTracts[i] = new wxStaticBitmap(panTractDisplay, wxID_ANY, wxBitmap(wxString::FromUTF8(getenv("HOME")) +
				wxString("/eclipse-workspace/ZasilanieMagnesow/Images/Trakt_") + wxString::Format(_T("%d.png"), i),
				wxBITMAP_TYPE_PNG), wxPoint(5, -2), wxSize(501, 666));
		imgTracts[i]->Hide();

		//Modbus Error Tract Image (Default)
		imgTractsMBError = new wxStaticBitmap(panTractDisplay, wxID_ANY, wxBitmap(wxString::FromUTF8(getenv("HOME")) +
				wxString("/eclipse-workspace/ZasilanieMagnesow/Images/Trakt_Error.png"),
				wxBITMAP_TYPE_PNG), wxPoint(5, -2), wxSize(501, 666));
		imgTractsMBError->Hide();

		//Buttons
		btnTractSelect[i] = new wxButton(panTractSelect, 10001 + i, TRACT_NAMES[i], wxPoint(17 + 68*i, 53), wxSize(58, 37));
		if (i == 0) btnTractSelect[i]->SetBackgroundColour(wxColour(56, 171, 255));
		else btnTractSelect[i]->SetBackgroundColour(wxColour(215, 215, 215));
		btnTractSelect[i]->SetFont(wxFont(10, wxFONTFAMILY_DEFAULT, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_BOLD));
	}

	//IMAGES
	imgZMKConnectedBox = new wxStaticBitmap(panTractDisplay, wxID_ANY, wxBitmap(wxString::FromUTF8(getenv("HOME")) +
				wxString("/eclipse-workspace/ZasilanieMagnesow/Images/ZMK_Outline.png"), wxBITMAP_TYPE_PNG), wxPoint(72, 562), wxSize(162, 72));

	imgZMKConnected = new wxStaticBitmap(panZMKConnected, wxID_ANY, wxBitmap(wxString::FromUTF8(getenv("HOME")) +
			wxString("/eclipse-workspace/ZasilanieMagnesow/Images/ZMK_Connected.png"), wxBITMAP_TYPE_PNG), wxPoint(98, 9), wxSize(55, 55));
	imgZMKConnected->Hide();

	imgZMKDisconnected = new wxStaticBitmap(panZMKConnected, wxID_ANY, wxBitmap(wxString::FromUTF8(getenv("HOME")) +
			wxString("/eclipse-workspace/ZasilanieMagnesow/Images/ZMK_Disconnected.png"), wxBITMAP_TYPE_PNG), wxPoint(100, 9), wxSize(55, 55));
	imgZMKDisconnected->Hide();

	//Bind tract switching buttons to single event handler
    Bind(wxEVT_BUTTON, &cMain::OnButtonSwitchTract, this, 10001, 10007);
    Bind(wxEVT_BUTTON, &cMain::OnButtonSetCurrent, this, 20001, 20012);

    //Display refresh timer
    tRefreshTimer = new wxTimer(this, 20013);
    tRefreshTimer->Start(DELAY_REFRESH_DISPLAY);
}

void cMain::RefreshDisplay(void)
{
	/******************************************* Copy Modbus Data ******************************************/

	if (mbMaster != nullptr)
	{
		wxCriticalSectionLocker local_lock(mbMaster->local_guard);

		bModbusError = mbMaster->bModbusError;
		bZMKConnected = mbMaster->bZMKConnected;
		ZNA[0] = mbMaster->ZNA[0];
		ZNA[1] = mbMaster->ZNA[1];

		for (uint8_t i = 0; i < NUM_POWER_UNITS; i++)
		{
			//Copy data
			bPSUTimeout[i] = mbMaster->bPSUTimeout[i];
			bPSUOverrun[i] = mbMaster->bPSUOverrun[i];
			if ((ZNA[1] & (1 << (i + 4))) != 0) fCurrent[i] =  mbMaster->fCurrent[i] * (-1);
			else fCurrent[i] =  mbMaster->fCurrent[i];
			fCurrentSetting[i] = mbMaster->fCurrentSetting[i];
		}
	}

	/******************************************* Update Tract Buttons **************************************/

	for (uint8_t i = 0; i < NUM_TRACTS + 1; i++)
	{
		if (eUserMode == Local)
		{
			if (!btnTractSelect[i]->IsEnabled()) btnTractSelect[i]->Enable();
			if (i != eActiveTract) btnTractSelect[i]->SetBackgroundColour(wxColour(215, 215, 215));
			else btnTractSelect[i]->SetBackgroundColour(wxColour(56, 171, 255));
		}
		else if (eUserMode == Remote)
		{
			if (btnTractSelect[i]->IsEnabled()) btnTractSelect[i]->Disable();
			if (i != eActiveTract) btnTractSelect[i]->SetBackgroundColour(wxColour(215, 215, 215));
			else btnTractSelect[i]->SetBackgroundColour(wxColour(56, 171, 255));
		}
	}

	/******************************************* New Tract Selection ***************************************/

	if (eActiveTract != eNewTract)
	{
		/******************************************* Delete MK-ZMK Panels **************************************/

		for (uint8_t i = 2; i < TRACTS[eActiveTract].size(); i++)
		{
			if (panMK[i] && TRACTS[eNewTract][i] != TRACTS[eActiveTract][i])
			{
				panMK[i]->Destroy();
				panMK[i] = NULL;
			}
		}

		/******************************************* Update Active Tract ***************************************/

		eActiveTract = eNewTract;

		/******************************************* Update Tract Display ***************************************/

		for (uint8_t i = 0; i < NUM_TRACTS + 1; i++)
		{
			//IMAGES
			if (i == eActiveTract && !imgTracts[i]->IsShown()) imgTracts[i]->Show();
			else if (imgTracts[i]->IsShown()) imgTracts[i]->Hide();
		}

		/******************************************* Update Tract Title ****************************************/

		if (eActiveTract != None) txtTitleControls->SetLabel(wxString(_T("Trakt: ")) + TRACT_NAMES[eActiveTract]);
		else txtTitleControls->SetLabel(wxString(_T("Brak Wybranego Traktu")));

		/******************************************* Draw MK-ZMK Panels ****************************************/

		for (uint8_t i = 0; i < TRACTS[eActiveTract].size(); i++)
		{
			mkNum = TRACTS[eActiveTract][i];

			if (!panMK[i])
			{
				panMK[i] = new wxPanel(panControls, wxID_ANY, wxPoint(10, 53 + i*125), wxSize(520, 110), wxSUNKEN_BORDER);

				//Draw panel divisor lines
				wxStaticLine* lineV1 = new wxStaticLine(panMK[i], wxID_ANY, wxPoint(90, 0), wxSize(1, 109), wxLI_VERTICAL);
				wxStaticLine* lineV2 = new wxStaticLine(panMK[i], wxID_ANY, wxPoint(195, 0), wxSize(1, 109), wxLI_VERTICAL);
				wxStaticLine* lineV3 = new wxStaticLine(panMK[i], wxID_ANY, wxPoint(290, 0), wxSize(1, 109), wxLI_VERTICAL);
				wxStaticLine* lineV4 = new wxStaticLine(panMK[i], wxID_ANY, wxPoint(460, 0), wxSize(1, 109), wxLI_VERTICAL);
				wxStaticLine* lineH = new wxStaticLine(panMK[i], wxID_ANY, wxPoint(91, 54), wxSize(428, 1), wxLI_HORIZONTAL);
				lineV1->SetBackgroundColour(wxColour(163, 168, 173));
				lineV2->SetBackgroundColour(wxColour(163, 168, 173));
				lineV3->SetBackgroundColour(wxColour(163, 168, 173));
				lineV4->SetBackgroundColour(wxColour(163, 168, 173));
				lineH->SetBackgroundColour(wxColour(163, 168, 173));

				//Draw MK Title
				if (mkNum == 21) //special case
				{
					wxStaticText* mkTitle = new wxStaticText(panMK[i], wxID_ANY, _T("MK: X2"), wxPoint(1, 47), wxSize(90, 20), wxALIGN_CENTRE_HORIZONTAL);
					mkTitle->SetFont(wxFont(10, wxFONTFAMILY_DEFAULT, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_BOLD));
				}
				else
				{
					wxStaticText* mkTitle = new wxStaticText(panMK[i], wxID_ANY, _T("MK: ") + wxString::Format(wxT("%i"), mkNum), wxPoint(1, 47), wxSize(90, 20), wxALIGN_CENTRE_HORIZONTAL);
					mkTitle->SetFont(wxFont(10, wxFONTFAMILY_DEFAULT, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_BOLD));
				}

				//Get ZMK
				zmkNum[0] = MK_ZMK[mkNum - 1][0];
				zmkNum[1] = MK_ZMK[mkNum - 1][1];

				//Draw ZMK information and controls
				for (uint8_t j = 0; j < MK_ZMK_NUM; j++)
				{
					//Draw ZMK title
					wxStaticText* zmkTitle = new wxStaticText(panMK[i], wxID_ANY, _T("Zasilacz ") + wxString::Format(wxT("%i"),
							zmkNum[j]), wxPoint(88, 19 + j*54), wxSize(110, 20), wxALIGN_CENTRE_HORIZONTAL);
					zmkTitle->SetFont(wxFont(10, wxFONTFAMILY_DEFAULT, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_NORMAL));

					//Draw current reading
					txtCurrentZMK[zmkNum[j] - 1] = new wxStaticText(panMK[i], wxID_ANY, wxString::Format(wxT("%.2fA"),
													fCurrent[zmkNum[j] - 1]), wxPoint(195, 18 + j*54), wxSize(100, 20), wxALIGN_CENTRE_HORIZONTAL);

					//Set current font
					txtCurrentZMK[zmkNum[j] - 1]->SetFont(wxFont(11, wxFONTFAMILY_DEFAULT, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_BOLD));

					//Draw current controls
					txtCtrlCurrentZMK[zmkNum[j] - 1] = new wxTextCtrl(panMK[i], wxID_ANY, wxString::Format(wxT("%.2f"),
							fCurrentSetting[zmkNum[j] - 1]), wxPoint(307, 13 + j*54), wxSize(70, 28), wxALIGN_CENTRE_HORIZONTAL);

					//Check user mode
					if (eUserMode == Remote)
					{
						if (txtCtrlCurrentZMK[zmkNum[j] - 1]->IsEnabled()) txtCtrlCurrentZMK[zmkNum[j] - 1]->Disable();
					}

					//Draw current units
					wxStaticText* txtCtrlCurrentUnits = new wxStaticText(panMK[i], wxID_ANY, "A",
							wxPoint(383, 19 + j*54), wxSize(40, 20), wxALIGN_LEFT);
					txtCtrlCurrentUnits->SetFont(wxFont(10, wxFONTFAMILY_DEFAULT, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_NORMAL));

					//Draw buttons
					btnCurrentZMK[zmkNum[j] - 1] = new wxButton(panMK[i], 20001 + zmkNum[j] - 1, "OK",
							wxPoint(404, 13 + j*54), wxSize(40, 28), wxBU_EXACTFIT);
					btnCurrentZMK[zmkNum[j] - 1]->SetBackgroundColour(wxColour(215, 215, 215));

					//Check user mode
					if (eUserMode == Remote)
					{
						if (btnCurrentZMK[zmkNum[j] - 1]->IsEnabled()) btnCurrentZMK[zmkNum[j] - 1]->Disable();
					}

					//Draw Modbus Error Images
					imgModbusError[zmkNum[j] - 1] = new wxStaticBitmap(panMK[i], wxID_ANY, wxBitmap(wxString::FromUTF8(getenv("HOME")) +
							wxString("/eclipse-workspace/ZasilanieMagnesow/Images/Modbus_Error.png"),
							wxBITMAP_TYPE_PNG), wxPoint(469, 8 + j*54), wxSize(40, 40));
					imgModbusError[zmkNum[j] - 1]->Hide();

					//Draw Current Error Images
					imgCurrentError[zmkNum[j] - 1] = new wxStaticBitmap(panMK[i], wxID_ANY, wxBitmap(wxString::FromUTF8(getenv("HOME")) +
							wxString("/eclipse-workspace/ZasilanieMagnesow/Images/ZMK_Error.png"),
							wxBITMAP_TYPE_PNG), wxPoint(469, 7 + j*54), wxSize(40, 40));
					imgCurrentError[zmkNum[j] - 1]->Hide();

					//Draw Current OK Images
					imgCurrentOK[zmkNum[j] - 1] = new wxStaticBitmap(panMK[i], wxID_ANY, wxBitmap(wxString::FromUTF8(getenv("HOME")) +
							wxString("/eclipse-workspace/ZasilanieMagnesow/Images/ZMK_Ready.png"),
							wxBITMAP_TYPE_PNG), wxPoint(469, 8 + j*54), wxSize(40, 40));
					imgCurrentOK[zmkNum[j] - 1]->Hide();

					//Draw Current Set Images
					imgCurrentSet[zmkNum[j] - 1] = new wxStaticBitmap(panMK[i], wxID_ANY, wxBitmap(wxString::FromUTF8(getenv("HOME")) +
							wxString("/eclipse-workspace/ZasilanieMagnesow/Images/ZMK_Busy.png"),
							wxBITMAP_TYPE_PNG), wxPoint(469, 8 + j*54), wxSize(40, 40));
					imgCurrentSet[zmkNum[j] - 1]->Hide();
				}
			}
		}
	}

	/******************************************* Error Messages *******************************************/

	if (bModbusError) //DISCONNECTED
	{
		//Display Modbus errors
		if (txtTitleTractSelect->GetPosition() != wxPoint(119, 20)) txtTitleTractSelect->SetPosition(wxPoint(119, 20));
		if (!txtModbusError->IsShown()) txtModbusError->Show();
		if (!txtZMKConnectedMBError->IsShown()) txtZMKConnectedMBError->Show();

		//Hide other errors
		if (txtSetTractError->IsShown()) txtSetTractError->Hide();

		//Hide ZMK info
		if (txtTitleZMKConnected->IsShown()) txtTitleZMKConnected->Hide();
		if (imgZMKConnected->IsShown()) imgZMKConnected->Hide();
		if (imgZMKDisconnected->IsShown()) imgZMKDisconnected->Hide();

		//Update tract image and buttons
		for (uint8_t i = 0; i < NUM_TRACTS + 1; i++)
		{
			if (imgTracts[i]->IsShown()) imgTracts[i]->Hide();
			if (btnTractSelect[i]->IsEnabled()) btnTractSelect[i]->Disable();
			btnTractSelect[i]->SetBackgroundColour(wxColour(220, 220, 220));
		}

		//Display empty tract diagram
		if (!imgTractsMBError->IsShown()) imgTractsMBError->Show();
	}
	else //CONNECTED
	{
		//Hide Modbus errors
		if (txtZMKConnectedMBError->IsShown()) txtZMKConnectedMBError->Hide();
		if (!txtTitleZMKConnected->IsShown()) txtTitleZMKConnected->Show();
		if (txtModbusError->IsShown()) txtModbusError->Hide();

		//Display ZMK info
		if (bZMKConnected)
		{
			txtTitleZMKConnected->SetPosition(wxPoint(6, 21));
			txtTitleZMKConnected->SetLabel(_T("Zasilanie\nWłączone"));
			txtTitleZMKConnected->SetForegroundColour(wxColour(34, 188, 34));
			if (!imgZMKConnected->IsShown()) imgZMKConnected->Show();
			if (imgZMKDisconnected->IsShown()) imgZMKDisconnected->Hide();
		}
		else
		{
			txtTitleZMKConnected->SetPosition(wxPoint(7, 20));
			txtTitleZMKConnected->SetLabel(_T("Zasilanie\nWyłączone"));
			txtTitleZMKConnected->SetForegroundColour(wxColour(207, 31, 37));
			if (!imgZMKDisconnected->IsShown()) imgZMKDisconnected->Show();
			if (imgZMKConnected->IsShown()) imgZMKConnected->Hide();
		}

		//Display or hide tract errors
		if (bSetTractError)
		{
			if (txtTitleTractSelect->GetPosition() != wxPoint(94, 20)) txtTitleTractSelect->SetPosition(wxPoint(94, 20));
			if (!txtSetTractError->IsShown()) txtSetTractError->Show();
		}
		else
		{
			if (txtTitleTractSelect->GetPosition() != wxPoint(175, 20)) txtTitleTractSelect->SetPosition(wxPoint(175, 20));
			if (txtSetTractError->IsShown()) txtSetTractError->Hide();
		}

		//Hide empty tract diagram
		if (imgTractsMBError->IsShown()) imgTractsMBError->Hide();

		//Draw tract diagram
		if (!imgTracts[eActiveTract]->IsShown()) imgTracts[eActiveTract]->Show();
	}

	/******************************************* Update MK-ZMK Panels **************************************/

	for (uint8_t i = 0; i < TRACTS[eActiveTract].size(); i++)
	{
		mkNum = TRACTS[eActiveTract][i];

		//Get ZMK
		zmkNum[0] = MK_ZMK[mkNum - 1][0];
		zmkNum[1] = MK_ZMK[mkNum - 1][1];

		for (uint8_t j = 0; j < MK_ZMK_NUM; j++)
		{
			if (bModbusError) //DISCONNECTED
			{
				if (imgCurrentOK[zmkNum[j] - 1]->IsShown()) imgCurrentOK[zmkNum[j] - 1]->Hide();
				if (imgCurrentError[zmkNum[j] - 1]->IsShown()) imgCurrentError[zmkNum[j] - 1]->Hide();
				if (!imgModbusError[zmkNum[j] - 1]->IsShown()) imgModbusError[zmkNum[j] - 1]->Show();

				//Disable buttons
				if (btnCurrentZMK[zmkNum[j] - 1]->IsEnabled()) btnCurrentZMK[zmkNum[j] - 1]->Disable();

				//Disable current input
				if (txtCtrlCurrentZMK[zmkNum[j] - 1]->IsEnabled() || eUserMode == Remote)
				{
					txtCurrentZMK[zmkNum[j] - 1]->SetLabel(wxString("-"));
					txtCtrlCurrentZMK[zmkNum[j] - 1]->SetValue(wxString("-"));
					txtCtrlCurrentZMK[zmkNum[j] - 1]->Disable();
				}
			}
			else //CONNECTED
			{
				//Check for current-setting errors
				if (!threadSetCurrent[zmkNum[j] - 1] && fabs(fCurrent[zmkNum[j] - 1] - fCurrentSetting[zmkNum[j] - 1]) >= MIN_SAFE_CURRENT)
				{
					bSetCurrentError[zmkNum[j] - 1] = true; //current setting error
				}
				else bSetCurrentError[zmkNum[j] - 1] = false; //clear  error

				//Hide Modbus errors
				if (imgModbusError[zmkNum[j] - 1]->IsShown()) imgModbusError[zmkNum[j] - 1]->Hide();

				if (eUserMode == Local)
				{
					//Enable/disable buttons
					if (!mbMaster->mbThreadWritePSU[zmkNum[j] - 1] && !threadSetCurrent[zmkNum[j] - 1]) //Modbus thread is not already working
					{
						if (!btnCurrentZMK[zmkNum[j] - 1]->IsEnabled()) btnCurrentZMK[zmkNum[j] - 1]->Enable();
					}
					else if (btnCurrentZMK[zmkNum[j] - 1]->IsEnabled()) btnCurrentZMK[zmkNum[j] - 1]->Disable();

					//Enable current input
					if (!txtCtrlCurrentZMK[zmkNum[j] - 1]->IsEnabled() && !threadSetTract)
					{
						txtCtrlCurrentZMK[zmkNum[j] - 1]->Enable();
						txtCtrlCurrentZMK[zmkNum[j] - 1]->SetValue(wxString::Format(wxT("%.2f"), fCurrentSetting[zmkNum[j] - 1]));
					}
				}
				else if (eUserMode == Remote)
				{
					if (btnCurrentZMK[zmkNum[j] - 1]->IsEnabled()) btnCurrentZMK[zmkNum[j] - 1]->Disable();
					if (txtCtrlCurrentZMK[zmkNum[j] - 1]->IsEnabled()) txtCtrlCurrentZMK[zmkNum[j] - 1]->Disable();

					//Remote set-tract active, zero current settings
					if (threadSetTract && wxAtof(txtCtrlCurrentZMK[zmkNum[j] - 1]->GetValue()) != 0)
					{
						txtCtrlCurrentZMK[zmkNum[j] - 1]->SetValue(_T("0.00"));
					}
				}

				//Update status icons and text
				if (bPSUTimeout[zmkNum[j] - 1] || bPSUOverrun[zmkNum[j] - 1])
				{
					if (imgModbusError[zmkNum[j] - 1]->IsShown()) imgModbusError[zmkNum[j] - 1]->Hide();
					if (imgCurrentOK[zmkNum[j] - 1]->IsShown()) imgCurrentOK[zmkNum[j] - 1]->Hide();
					if (imgCurrentSet[zmkNum[j] - 1]->IsShown()) imgCurrentSet[zmkNum[j] - 1]->Hide();
					if (!imgCurrentError[zmkNum[j] - 1]->IsShown()) imgCurrentError[zmkNum[j] - 1]->Show();

					//Cannot read current
					txtCurrentZMK[zmkNum[j] - 1]->SetLabel(wxString("-"));
				}
				else if (bSetCurrentError[zmkNum[j] - 1])
				{
					if (imgModbusError[zmkNum[j] - 1]->IsShown()) imgModbusError[zmkNum[j] - 1]->Hide();
					if (imgCurrentOK[zmkNum[j] - 1]->IsShown()) imgCurrentOK[zmkNum[j] - 1]->Hide();
					if (imgCurrentSet[zmkNum[j] - 1]->IsShown()) imgCurrentSet[zmkNum[j] - 1]->Hide();
					if (!imgCurrentError[zmkNum[j] - 1]->IsShown()) imgCurrentError[zmkNum[j] - 1]->Show();

					//Update current
					txtCurrentZMK[zmkNum[j] - 1]->SetLabel(wxString::Format(wxT("%.2fA"), fCurrent[zmkNum[j] - 1]));
				}
				else if (mbMaster->mbThreadWritePSU[zmkNum[j] - 1] || threadSetCurrent[zmkNum[j] - 1]) //ZMK busy
				{
					if (imgModbusError[zmkNum[j] - 1]->IsShown()) imgModbusError[zmkNum[j] - 1]->Hide();
					if (imgCurrentOK[zmkNum[j] - 1]->IsShown()) imgCurrentOK[zmkNum[j] - 1]->Hide();
					if (imgCurrentError[zmkNum[j] - 1]->IsShown()) imgCurrentError[zmkNum[j] - 1]->Hide();
					if (!imgCurrentSet[zmkNum[j] - 1]->IsShown()) imgCurrentSet[zmkNum[j] - 1]->Show();

					//Update current
					txtCurrentZMK[zmkNum[j] - 1]->SetLabel(wxString::Format(wxT("%.2fA"), fCurrent[zmkNum[j] - 1]));
				}
				else //No Errors, display Current OK
				{
					if (imgModbusError[zmkNum[j] - 1]->IsShown()) imgModbusError[zmkNum[j] - 1]->Hide();
					if (imgCurrentError[zmkNum[j] - 1]->IsShown()) imgCurrentError[zmkNum[j] - 1]->Hide();
					if (imgCurrentSet[zmkNum[j] - 1]->IsShown()) imgCurrentSet[zmkNum[j] - 1]->Hide();
					if (!imgCurrentOK[zmkNum[j] - 1]->IsShown()) imgCurrentOK[zmkNum[j] - 1]->Show();

					//Update current
					txtCurrentZMK[zmkNum[j] - 1]->SetLabel(wxString::Format(wxT("%.2fA"), fCurrent[zmkNum[j] - 1]));
				}
			}
		}
	}

	//Refresh and update frame
	this->Refresh();
	this->Update();
}

void cMain::OnRefreshDisplay(wxTimerEvent& evt)
{
	RefreshDisplay();
}

void cMain::OnEraseBackground(wxEraseEvent& event)
{
	return; //do nothing
}

/* Menu clicked event */
void cMain::OnMenuClickedInformation(wxCommandEvent& evt)
{
    if (informationWindowIsOpen) //window already open
    {
        pInformation->Raise(); //show window
    }
    else
    {
        informationWindowIsOpen = true;
        pInformation = new cInformation(this);
        pInformation->SetPosition(this->GetPosition());
        pInformation->SetIcon(this->GetIcon());
        pInformation->Show();
    }

    evt.Skip();
}

/* Menu clicked event */
void cMain::OnMenuClickedMode(wxCommandEvent& evt)
{
	/************************************ Update Frame Title ***********************************/

	if (eUserMode == Local)
	{
	    this->SetTitle(_T("Zasilanie Magnesów Korekcyjnych (Sterowanie Zdalne)"));
		mMenuBar->SetMenuLabel(0, _T("Sterowanie: Zdalne"));
		mModeMenu->SetLabel(wxID_NETWORK, _T("Przejdź na Sterowanie Lokalne"));
		eUserMode = Remote;
	}
	else if (eUserMode == Remote)
	{
	    this->SetTitle(_T("Zasilanie Magnesów Korekcyjnych"));
		mMenuBar->SetMenuLabel(0, _T("Sterowanie: Lokalne"));
		mModeMenu->SetLabel(wxID_NETWORK, _T("Przejdź na Sterowanie Zdalne"));
		eUserMode = Local;
	}

	/************************************ Remote Data ******************************************/

	if (mbMaster != nullptr)
	{
		wxCriticalSectionLocker remote_lock(mbMaster->remote_guard);
		mbMaster->mb_mapping->tab_input_registers[MB_INPUTREG_USERMODE - 1] = (uint16_t) (eUserMode == Local);
	}

    evt.Skip();
}

/* Frame closed */
void cMain::OnClose(wxCloseEvent&)
{
	TerminateApp(false);
}

/* Frame closed from menu */
void cMain::OnMenuClickedExit(wxCommandEvent& evt)
{
	TerminateApp(false);
}

void cMain::TerminateApp(bool bInitError)
{
	/******************************************* Stop Refresh  *********************************************/

	tRefreshTimer->Stop();

	/******************************************* Delete Set-Tract Thread ***********************************/

	if (threadSetTract) threadSetTract->Delete();
	while (threadSetTract != NULL) wxMilliSleep(1);

	if (!bInitError)
	{
		/******************************************* Copy Modbus Data ******************************************/

		if (mbMaster != nullptr)
		{
			wxCriticalSectionLocker local_lock(mbMaster->local_guard);
			bModbusError = mbMaster->bModbusError;
			bZMKConnected = mbMaster->bZMKConnected;
			for (uint8_t i = 0; i < NUM_POWER_UNITS; i++) fCurrentSetting[i] = mbMaster->fCurrentSetting[i];
		}

		if (!bModbusError && mbMaster != nullptr)
		{
			/******************************************* Zero Current **********************************************/

			//Zero current setting displays
			for (uint8_t i = 0; i < TRACTS[eActiveTract].size(); i++)
			{
				mkNum = TRACTS[eActiveTract][i];

				//Get ZMK
				zmkNum[0] = MK_ZMK[mkNum - 1][0];
				zmkNum[1] = MK_ZMK[mkNum - 1][1];

				for (uint8_t j = 0; j < MK_ZMK_NUM; j++) txtCtrlCurrentZMK[zmkNum[j] - 1]->SetValue(_T("0.00"));
			}

			//Terminate any active set-current threads
			for (uint8_t i = 0; i < NUM_POWER_UNITS; i++)
			{
				if (threadSetCurrent[i] != NULL)
				{
					threadSetCurrent[i]->Delete();
					while (threadSetCurrent[i] != NULL) wxMilliSleep(1);
				}

				if (mbMaster->mbThreadWritePSU[i] != NULL)
				{
					mbMaster->mbThreadWritePSU[i]->Delete();
					while (mbMaster->mbThreadWritePSU[i] != NULL) wxMilliSleep(1);
				}
			}

			//Start new thread that handles tract re-configuration
			threadSetTract = new cThreadSetTract(this, mbMaster, None);

			if (threadSetTract->Run() != wxTHREAD_NO_ERROR)
			{
				wxLogError("Can't create set-current thread!");
				delete threadSetTract;
				threadSetTract = NULL;
			}

			//Reset timer
			TimerCloseApp->reset();

			//Wait until currents reach safety limits
			while (!bCurrentOK && !bModbusError && TimerCloseApp->elapsed() <= CLOSE_APP_TIMEOUT)
			{
				//Reset flag
				bCurrentOK = true;

				//Check current readings
				for (uint8_t i = 0; i < NUM_POWER_UNITS; i++)
				{
					if (fabs(fCurrent[i]) >= MIN_SAFE_CURRENT || bPSUTimeout[i] || bPSUOverrun[i])
					{
						bCurrentOK = false;
						break;
					}
				}

				wxMilliSleep(DELAY_REFRESH_DISPLAY);
				RefreshDisplay(); //updates Modbus data as well
			}

			if (!bCurrentOK)
			{
				//Display user dialog
				if (wxMessageBox(_T("\nBłąd w zerowaniu prądu w zasilaczach.\nKoniec Pracy?"),
						_T("Koniec Pracy"), wxOK | wxCANCEL | wxICON_ERROR) != wxOK)
				{
					//Restart refresh timer
					tRefreshTimer->Start(DELAY_REFRESH_DISPLAY);

					return;
				}
			}

			//Wait for set-tract thread to finish
			while (threadSetTract != NULL && mbMaster->mbThreadSetActiveTract != NULL) wxMilliSleep(1);

			//Wait for all set-current threads to finish
			for (uint8_t i = 0; i < NUM_POWER_UNITS; i++)
			{
				while (mbMaster->mbThreadWritePSU[i] != NULL) wxMilliSleep(1);
				while (threadSetCurrent[i] != NULL) wxMilliSleep(1);
			}

			RefreshDisplay(); //updates Modbus data as well

			if (bZMKConnected)
			{
				this->Hide();
		        pPowerCheck = new cPowerCheck(mbMaster);
		        pPowerCheck->SetIcon(wxICON(icon));
		        pPowerCheck->Show();
		        pPowerCheck->SetFocus();
			}
			else wxMilliSleep(DELAY_CLOSE_APP); //close app after delay
		}
	}
	else
	{
		//Wait for set-tract thread to finish
		while (threadSetTract != NULL && mbMaster->mbThreadSetActiveTract != NULL) wxMilliSleep(1);

		//Wait for all set-current threads to finish
		for (uint8_t i = 0; i < NUM_POWER_UNITS; i++)
		{
			while (mbMaster->mbThreadWritePSU[i] != NULL) wxMilliSleep(1);
			while (threadSetCurrent[i] != NULL) wxMilliSleep(1);
		}
	}

	/******************************************* Close Windows *********************************************/

	if (informationWindowIsOpen) pInformation->Close();

	/******************************************* Clear Modbus Pointer **************************************/

	if (mbMaster != nullptr) mbMaster->pMain = NULL;

	/******************************************* Destroy Frame *********************************************/

	Destroy();
}
