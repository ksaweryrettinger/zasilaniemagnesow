#include "cThreadSetTract.h"

cThreadSetTract::cThreadSetTract(cMain* pMain, cModbus* mbMaster, uint8_t tractID) : wxThread(wxTHREAD_DETACHED)
{
	//Copy parameters
	this->pMain = pMain;
	this->mbMaster = mbMaster;
	this->eNewTract = (eTract) tractID;

	//Initialize class members
	mkNum = 0;
	bTimeout = false;
	bCurrentOK = false;
	bModbusError = false;
	TimerSwitchTract = new cTimerHighRes();

	//Refresh data
	RefreshMainData();
	RefreshModbusData();
}

cThreadSetTract::~cThreadSetTract()
{
	wxCriticalSectionLocker main_lock(pMain->main_guard);
	pMain->threadSetTract = NULL;
}

wxThread::ExitCode cThreadSetTract::Entry()
{
	//Signal Modbus layer to read all PSUs during tract switch
	if (mbMaster->pMain != NULL)
	{
		wxCriticalSectionLocker main_lock(pMain->main_guard);
		pMain->bTractSwitching = true;
	}

	//Zero current on all PSUs
	for (uint8_t i = 0; i < NUM_POWER_UNITS; i++)
	{
		if (fabs(fCurrentSetting[i]) >= MIN_SAFE_CURRENT)
		{
			//Start new set-current thread
			pMain->threadSetCurrent[i] = new cThreadSetCurrent(pMain, mbMaster, i + 1, 0);

			if (pMain->threadSetCurrent[i]->Run() != wxTHREAD_NO_ERROR)
			{
				wxLogError("Can't create set-current thread!");
				delete pMain->threadSetCurrent[i];
				pMain->threadSetCurrent[i] = NULL;
			}
		}
	}

	//Start timer
	TimerSwitchTract->reset();

	if (CHECK_TRACT_ERRORS)
	{
		//Check current
		while (!bCurrentOK)
		{
			wxThread::Sleep(DELAY_TRACT_SWITCH);

			RefreshModbusData();
			if (TestErrors()) return (wxThread::ExitCode) 0;

			bCurrentOK = true;

			//Check current on active units
			for (uint8_t i = 0; i < NUM_POWER_UNITS; i++)
			{
				if (fCurrent[i] >= MIN_SAFE_CURRENT) //current > 0.1A
				{
					bCurrentOK = false;
					break;
				}

				if (!bCurrentOK) break;
			}
		}
	}

	//Change active PSUs and zero polarity bits
    mbMaster->SetActiveTract(eNewTract);

    //Signal Modbus layer to read only active PSUs and reset errors
	if (mbMaster->pMain != NULL)
	{
		wxCriticalSectionLocker main_lock(pMain->main_guard);
		pMain->bTractSwitching = false;
		pMain->bSetTractError = false;
	}

	return (wxThread::ExitCode) 0;
}

void cThreadSetTract::RefreshModbusData(void)
{
	//Copy Modbus data
	wxCriticalSectionLocker local_lock(mbMaster->local_guard);

	bModbusError = mbMaster->bModbusError;

	for (uint8_t i = 0; i < NUM_POWER_UNITS; i++)
	{
		bPSUTimeout[i] = mbMaster->bPSUTimeout[i];
		bPSUOverrun[i] = mbMaster->bPSUOverrun[i];
		fCurrent[i] = mbMaster->fCurrent[i];
		fCurrentSetting[i] = mbMaster->fCurrentSetting[i];
	}
}

void cThreadSetTract::RefreshMainData(void)
{
	wxCriticalSectionLocker main_lock(pMain->main_guard);

	//Copy Main data
	eActiveTract = pMain->eActiveTract;
}

bool cThreadSetTract::TestErrors(void)
{
	//Terminate thread if destroyed externally
	if (TestDestroy()) return true;

	//Terminate thread on Modbus connection errors
	if (bModbusError) return true;

	//Terminate thread on timeout
	if (TimerSwitchTract->elapsed() > SWITCH_TRACT_TIMEOUT)
	{
		wxCriticalSectionLocker main_lock(pMain->main_guard);
		pMain->bSetTractError = true;
		return true;
	}

	//Terminate thread in the event of read-current errors
	for (uint8_t i = 0; i < NUM_POWER_UNITS; i++)
	{
		if (bPSUOverrun[i] || bPSUTimeout[i])
		{
			wxCriticalSectionLocker main_lock(pMain->main_guard);
			pMain->bSetTractError = true;
			return true;
		}
	}

	//No errors
	return false;
}

