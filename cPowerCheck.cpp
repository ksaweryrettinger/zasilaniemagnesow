#include "cPowerCheck.h"

wxBEGIN_EVENT_TABLE(cPowerCheck, wxFrame)
	EVT_TIMER(20014, cPowerCheck::OnRefreshDisplay)
wxEND_EVENT_TABLE()

/* Frame constructor */
cPowerCheck::cPowerCheck(cModbus* mbMaster) : wxFrame(nullptr, wxID_ANY, " ", wxPoint(420, 400), wxSize(450, 200), (wxDEFAULT_FRAME_STYLE & ~wxRESIZE_BORDER & ~wxMAXIMIZE_BOX & ~wxCLOSE_BOX & ~wxMINIMIZE_BOX))
{
    //FRAME TITLE AND COLOUR
    this->SetTitle(_T("Zasilanie Główne"));
    this->SetFont(wxFont(11, wxFONTFAMILY_DEFAULT, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_NORMAL));
    this->SetBackgroundColour(wxColour(230, 230, 230));

    //CONSTRUCTOR VARIABLES
    this->mbMaster = mbMaster;

	//OTHER VARIABLES
    bModbusError = false;
	bZMKConnected = false;

	//WARNING TEXT AND IMAGE
	txtWarning = new wxStaticText(this, wxID_ANY, _T("Uwaga: Zasilanie włączone!\n\nAby zakończyć pracę,\nwyłącz zasilanie."), wxPoint(147, 50), wxSize(300, 100), wxALIGN_CENTRE_HORIZONTAL);
	txtWarning->SetFont(wxFont(12, wxFONTFAMILY_DEFAULT, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_BOLD));
	txtWarning->SetForegroundColour(wxColour(207, 31, 37));

	imgWarning = new wxStaticBitmap(this, wxID_ANY, wxBitmap(wxString::FromUTF8(getenv("HOME")) +
					wxString("/eclipse-workspace/ZasilanieMagnesow/Images/Warning.png"),
					wxBITMAP_TYPE_PNG), wxPoint(20, 22), wxSize(128, 128));

    //Display refresh timer
    tRefreshTimer = new wxTimer(this, 20014);
    tRefreshTimer->Start(DELAY_REFRESH_DISPLAY);
}

void cPowerCheck::RefreshDisplay(void)
{
	if (mbMaster != nullptr)
	{
		wxCriticalSectionLocker local_lock(mbMaster->local_guard);
		bModbusError = mbMaster->bModbusError;
		bZMKConnected = mbMaster->bZMKConnected;
	}

	if (bModbusError || !bZMKConnected) TerminateApp();
}

void cPowerCheck::OnRefreshDisplay(wxTimerEvent& evt)
{
	RefreshDisplay();
}

void cPowerCheck::TerminateApp(void)
{
	tRefreshTimer->Stop();

	if (bModbusError)
	{
		txtWarning->SetLabel(_T("\nBłąd Modbus!\n"));

		//Refresh and update frame
		this->Refresh();
		this->Update();

		//Delay close app
		wxThread::Sleep(2500);
	}
	else
	{
		//Refresh and update frame
		this->Refresh();
		this->Update();
	}

	Destroy();
}
