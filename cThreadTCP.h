#pragma once

#include "ZMKConstants.h"
#include "cModbus.h"

class cModbus;

DECLARE_EVENT_TYPE(wxEVT_REMOTE_SET_TRACT, -1)
DECLARE_EVENT_TYPE(wxEVT_REMOTE_SET_CURRENT, -1)

/* Modbus TCP/IP thread */
class cThreadTCP : public wxThread
{
	public:
		cThreadTCP(cModbus*);
		~cThreadTCP();

	protected:
		virtual ExitCode Entry();

	private:
		void RestartSocketTCP(modbus_t*);

	private: //TCP/IP configuration
		cModbus* mbMaster;
		uint8_t *tcpQuery;
		modbus_t* ctxTCP;
		int32_t socket;
		int32_t rc;
		int32_t rp;
		int32_t ct;

	private:
		int32_t unitID;
		uint16_t regHoldingTemp;
		eTract eActiveTract;
		int32_t regNum;
		int32_t regStart;
		uint8_t mkNum;
		uint8_t zmkNum[2] = { 0 };
		char cCurrent[6] = { 0 };
		bool bTractSet;
		bool bCurrentSet;
};
