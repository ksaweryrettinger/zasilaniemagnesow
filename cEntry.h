#pragma once

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunused-variable"
#pragma GCC diagnostic ignored "-Wwrite-strings"

#include "ZMKConstants.h"
#include "Images/Icon.xpm"
#include "cMain.h"
#include "cMainError.h"
#include "cModbus.h"
#include "cThreadSetCurrent.h"

class cMain;
class cMainError;
class cModbus;
class cTimerHighRes;

class cEntry : public wxApp
{
public:
	cEntry();

public:
    virtual bool OnInit() override;
    virtual int OnExit() override;

public: //class pointers
    cMain* pMain;
    cMainError* pMainError;
    cModbus* mbMaster;
    wxSingleInstanceChecker* wxChecker;

private: //Modbus configuration
    int32_t mbAddress;
    wxString mbPort;

private: //Bit Register addresses
	int32_t ZMK_Address[NUM_POWER_UNITS] = { 0 };
	int32_t ZNA_Address[2] = { 0 };
	int32_t ODC_Address;
	int32_t ZAP_Address;

private: //Configuration file variables
    wxTextFile tConfigFile;
    wxString filename;
    wxString strConfig;

private: //other variables
    int32_t dlgUpdate;
    wxString temp;
    cTimerHighRes* TimerInit;
    bool bConfigError;

    friend class cModbus;
};

#pragma GCC diagnostic pop
