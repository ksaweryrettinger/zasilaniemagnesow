#pragma once

#include "ZMKConstants.h"
#include "cModbus.h"

class cMain;
class cModbus;
class cThreadReadPSU;
class cThreadWritePSU;
class cThreadSetPolarity;
class cThreadSetActiveTract;
class cTimerHighRes;

/* Looping thread, used to read Modbus holding registers */
class cThreadReadPSU : public wxThread
{
	public:
		cThreadReadPSU(cModbus*);
		~cThreadReadPSU();
		bool InitialisePSU(void);

	protected:
		virtual ExitCode Entry();

	private:
		void UpdateModbusData(void);

	private:
		cModbus* mbMaster;
		int32_t rc;
		int32_t ct;

	private:
		cTimerHighRes* TimerInitPSU;
		cTimerHighRes* TimerReadCurrent;
		eTract eActiveTract;
		uint8_t mkNum;
		uint8_t zmkNum[2] = { 0 };
		uint16_t zmkZap;
		uint16_t rdBuffer;
		uint16_t ZNA[2] = { 0 };
		uint16_t ZMKConnected;
		wxString strCurrent;
		float fCurrent[NUM_POWER_UNITS] = { 0 };
		bool bPSUTimeout[NUM_POWER_UNITS] = { false };
		bool bPSUOverrun[NUM_POWER_UNITS] = { false };
		bool rdStatus;
		bool bModbusError;
		bool bInitSuccess;
		bool bTractSwitching;
};

/* Temporary thread, used to switch active PSUs */
class cThreadSetActiveTract : public wxThread
{
	public:
		cThreadSetActiveTract(cModbus*, eTract);
		~cThreadSetActiveTract();

	protected:
		virtual ExitCode Entry();
		cModbus* mbMaster;

	private:
		eTract eNewTract;
		uint8_t mkNum;
		uint16_t ZNA[2] = { 0 };
		int32_t rc;
		bool bModbusError;
};

/* Temporary thread, used to write new current value to PSU */
class cThreadWritePSU : public wxThread
{
	public:
		cThreadWritePSU(cModbus*, uint8_t, float);
		~cThreadWritePSU();

	protected:
		virtual ExitCode Entry();
		cModbus* mbMaster;

	private:
		uint8_t unitID;
		int32_t rc;
		wxString strCurrentSetting;
		float fNewCurrentSetting;
		bool bNegativePolarity;

	private: //local data
		bool bModbusError;
		float fCurrentSetting;
		uint16_t ZNA[2] = { 0 };
};

/* Temporary thread, used to set new PSU polarity */
class cThreadSetPolarity : public wxThread
{
	public:
		cThreadSetPolarity(cModbus*, uint8_t, bool);
		~cThreadSetPolarity();

	protected:
		virtual ExitCode Entry();
		cModbus* mbMaster;

	private:
		bool bSetNegative;
		bool bModbusError;
		int32_t rc;
		uint16_t ZNA[2] = { 0 };
		uint8_t unitID;
};

