#include "cThreadSetCurrent.h"

/*	This thread does the following:
 *
 *	  - Check if polarity changed.
 *  	- If polarity didn't change, send command with new fCurrentSetting setting.
 *  	- If polarity did change:
 *  		- Set fCurrentSetting to zero and wait until it reaches zero.
 *  		- Change polarity of power unit.
 *			- Apply new fCurrentSetting value.
 */

cThreadSetCurrent::cThreadSetCurrent(cMain* pMain, cModbus* mbMaster, uint8_t unitID, float fNewCurrent) : wxThread(wxTHREAD_DETACHED)
{
	this->pMain = pMain;
	this->mbMaster = mbMaster;
	this->unitID = unitID;
	this->fNewCurrent = fNewCurrent;
	bTimeout = false;
	TimerSetCurrent = new cTimerHighRes();

	RefreshModbusData();
}

cThreadSetCurrent::~cThreadSetCurrent()
{
	wxCriticalSectionLocker main_lock(pMain->main_guard);
	pMain->threadSetCurrent[unitID - 1] = NULL;
}

wxThread::ExitCode cThreadSetCurrent::Entry()
{
	if ((fCurrentSetting >= 0 && fNewCurrent < 0) || (fCurrentSetting < 0 && fNewCurrent >= 0)) //polarity changed
	{
		//Set current to zero
		mbMaster->SetPowerUnitCurrent(unitID, 0);

		//Start timeout timer
		TimerSetCurrent->reset();

		//Wait for current to reach zero
		while (fabs(fCurrentSetting) >= MIN_SAFE_CURRENT)
		{
			wxThread::Sleep(DELAY_SET_CURRENT);
			RefreshModbusData();
			if (TestErrors()) return (wxThread::ExitCode) 0;
		}

		//Switch polarity and set new current
		mbMaster->SetPowerUnitPolarity(unitID, (fNewCurrent < 0));
		while (mbMaster->mbThreadSetPolarity[unitID - 1] != NULL) wxThread::Sleep(1);
		mbMaster->SetPowerUnitCurrent(unitID, fabs(fNewCurrent));
	}
	else if (fNewCurrent != fCurrentSetting)//POLARITY UNCHANGED
	{
		mbMaster->SetPowerUnitCurrent(unitID, fabs(fNewCurrent)); //apply new current (absolute value)
	}

	//Start timeout timer
	TimerSetCurrent->reset();

	//Wait for current to reach new setting
	while (fabs(fCurrent - fNewCurrent) >= MIN_SAFE_CURRENT)
	{
		wxThread::Sleep(DELAY_SET_CURRENT);
		RefreshModbusData();
		if (TestErrors()) return (wxThread::ExitCode) 0;
	}

	//Clear polarity for null current settings
	if (fNewCurrent == 0)
	{
		mbMaster->SetPowerUnitPolarity(unitID, false);
		while (mbMaster->mbThreadSetPolarity[unitID - 1] != NULL) wxThread::Sleep(1);
	}

	return (wxThread::ExitCode) 0;
}

void cThreadSetCurrent::RefreshModbusData(void)
{
	//Copy Modbus data
	wxCriticalSectionLocker local_lock(mbMaster->local_guard);

	bModbusError = mbMaster->bModbusError;
	bPSUTimeout = mbMaster->bPSUTimeout[unitID - 1];
	bPSUOverrun = mbMaster->bPSUOverrun[unitID - 1];
	fCurrent =  mbMaster->fCurrent[unitID - 1];
	fCurrentSetting = mbMaster->fCurrentSetting[unitID - 1];
	ZNA[1] = mbMaster->ZNA[1];

	if ((ZNA[1] & (1 << (unitID + 3))) != 0) fCurrent =  mbMaster->fCurrent[unitID - 1] * (-1);
	else fCurrent =  mbMaster->fCurrent[unitID - 1];
}

bool cThreadSetCurrent::TestErrors(void)
{
	//Terminate thread if destroyed externally
	if (TestDestroy()) return true;

	//Terminate thread on Modbus connection errors
	if (bModbusError) return true;

	//Terminate thread on timeout
	if (TimerSetCurrent->elapsed() > SET_CURRENT_TIMEOUT) return true;

	//Terminate thread in the event of read-current errors
	if (bPSUOverrun || bPSUTimeout) return true;

	//No errors
	return false;
}

