#include "cMainError.h"

wxBEGIN_EVENT_TABLE(cMainError, wxFrame)
	EVT_MENU(wxID_CLOSE, cMainError::OnMenuClickedExit)
	EVT_TIMER(20013, cMainError::OnRefreshDisplay)
    EVT_CLOSE(cMainError::OnClose)
wxEND_EVENT_TABLE()

/* Frame constructor */
cMainError::cMainError(cEntry* pEntry) : wxFrame(nullptr, wxID_ANY, " ", wxPoint(330, 250), wxSize(600, 522), (wxDEFAULT_FRAME_STYLE & ~wxRESIZE_BORDER & ~wxMAXIMIZE_BOX))
{
    //FRAME TITLE AND COLOUR
    this->SetTitle(_T("Zasilanie Magnesów Korekcyjnych"));
    this->SetFont(wxFont(11, wxFONTFAMILY_DEFAULT, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_NORMAL));
    this->SetBackgroundColour(wxColour(230, 230, 230));

    //CONSTRUCTOR VARIABLES
    mbMaster = nullptr;
    this->pEntry = pEntry;

    //Other variables initialization
    bModbusError = false;
    bCurrentOK = false;

	//MENUS
	mMenuBar = new wxMenuBar();
	mMenuBar->SetFont(wxFont(9, wxFONTFAMILY_DEFAULT, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_NORMAL));
	mMenuBar->SetBackgroundColour(wxColour(221, 220, 220));
	mHelpMenu = new wxMenu();
	mCloseMenu = new wxMenu();

	if (mHelpMenu != nullptr)
	{
		mCloseMenu->Append(wxID_CLOSE, _T("Zakończ"));
		mMenuBar->Append(mCloseMenu, _T("Koniec Pracy"));
		SetMenuBar(mMenuBar);
	}

	//TITLE
	txtTitleMain = new wxStaticText(this, wxID_ANY, _T("Błąd w Inicjalizacji Zasilaczy"), wxPoint(0, 21), wxSize(600, 20), wxALIGN_CENTRE_HORIZONTAL);
	txtTitleMain->SetFont(wxFont(11, wxFONTFAMILY_DEFAULT, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_BOLD));
	txtTitleMain->SetForegroundColour(wxColour(207, 31, 37));

	//PANELS
	for (uint8_t i = 0; i < NUM_POWER_UNITS; i++)
	{
		if (i < 6) panZMK[i] = new wxPanel(this, wxID_ANY, wxPoint(30, 60 + i*65), wxSize(260, 55), wxSUNKEN_BORDER);
		else panZMK[i] = new wxPanel(this, wxID_ANY, wxPoint(310, 60 + (i - 6)*65), wxSize(260, 55), wxSUNKEN_BORDER);

		//Draw panel divisor lines
		wxStaticLine* lineV1 = new wxStaticLine(panZMK[i], wxID_ANY, wxPoint(115, 0), wxSize(1, 54), wxLI_VERTICAL);
		wxStaticLine* lineV2 = new wxStaticLine(panZMK[i], wxID_ANY, wxPoint(200, 0), wxSize(1, 54), wxLI_VERTICAL);
		lineV1->SetBackgroundColour(wxColour(186, 186, 186));
		lineV2->SetBackgroundColour(wxColour(186, 186, 186));

		//Draw ZMK Title
		wxStaticText* zmkTitle = new wxStaticText(panZMK[i], wxID_ANY, _T("Zasilacz: ") + wxString::Format(wxT("%i"), i + 1),
				wxPoint(14, 19), wxSize(90, 20), wxALIGN_CENTRE_HORIZONTAL);
		zmkTitle->SetFont(wxFont(10, wxFONTFAMILY_DEFAULT, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_NORMAL));

		//Draw current reading
		txtCurrentZMK[i] = new wxStaticText(panZMK[i], wxID_ANY, wxString::Format(wxT("%.2fA"),
				fCurrent[i]), wxPoint(108, 18), wxSize(100, 20), wxALIGN_CENTRE_HORIZONTAL);
		txtCurrentZMK[i]->SetFont(wxFont(11, wxFONTFAMILY_DEFAULT, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_BOLD));

		//Draw Modbus Error Images
		imgModbusError[i] = new wxStaticBitmap(panZMK[i], wxID_ANY, wxBitmap(wxString::FromUTF8(getenv("HOME")) +
				wxString("/eclipse-workspace/ZasilanieMagnesow/Images/Modbus_Error.png"),
				wxBITMAP_TYPE_PNG), wxPoint(209, 8), wxSize(40, 40));
		imgModbusError[i]->Hide();

		//Draw Current Error Images
		imgCurrentError[i] = new wxStaticBitmap(panZMK[i], wxID_ANY, wxBitmap(wxString::FromUTF8(getenv("HOME")) +
				wxString("/eclipse-workspace/ZasilanieMagnesow/Images/ZMK_Error.png"),
				wxBITMAP_TYPE_PNG), wxPoint(209, 7), wxSize(40, 40));
		imgCurrentError[i]->Hide();

		//Draw Current OK Images
		imgCurrentOK[i] = new wxStaticBitmap(panZMK[i], wxID_ANY, wxBitmap(wxString::FromUTF8(getenv("HOME")) +
				wxString("/eclipse-workspace/ZasilanieMagnesow/Images/ZMK_Ready.png"),
				wxBITMAP_TYPE_PNG), wxPoint(209, 8), wxSize(40, 40));
		imgCurrentOK[i]->Hide();
	}

    //Display refresh timer
    tRefreshTimer = new wxTimer(this, 20013);
    tRefreshTimer->Start(DELAY_REFRESH_DISPLAY);
}

void cMainError::RefreshDisplay(void)
{
	/******************************************* Copy Modbus Data ******************************************/

	if (mbMaster != nullptr)
	{
		wxCriticalSectionLocker local_lock(mbMaster->local_guard);

		bModbusError = mbMaster->bModbusError;

		for (uint8_t i = 0; i < NUM_POWER_UNITS; i++)
		{
			//Copy data
			bPSUTimeout[i] = mbMaster->bPSUTimeout[i];
			bPSUOverrun[i] = mbMaster->bPSUOverrun[i];
			bInitTimeout[i] = mbMaster->bInitTimeout[i];
			fCurrent[i] = mbMaster->fCurrent[i];
		}
	}

	/******************************************* Update Panels *********************************************/

	bCurrentOK = true;

	for (uint8_t i = 0; i < NUM_POWER_UNITS; i++)
	{
		if (bInitTimeout[i] && fCurrent[i] < MIN_SAFE_CURRENT) bInitTimeout[i] = false; //CLEAR TIMEOUT ERROR

		if (bModbusError) //DISCONNECTED
		{
			if (imgCurrentOK[i]->IsShown()) imgCurrentOK[i]->Hide();
			if (imgCurrentError[i]->IsShown()) imgCurrentError[i]->Hide();
			if (!imgModbusError[i]->IsShown()) imgModbusError[i]->Show();
			txtCurrentZMK[i]->SetLabel(wxString("-"));

			//Current errors
			bCurrentOK = false;
		}
		else //CONNECTED
		{
			//Hide Modbus errors
			if (imgModbusError[i]->IsShown()) imgModbusError[i]->Hide();

			//Display errors
			if (bPSUTimeout[i] || bPSUOverrun[i])
			{
				if (imgModbusError[i]->IsShown()) imgModbusError[i]->Hide();
				if (imgCurrentOK[i]->IsShown()) imgCurrentOK[i]->Hide();
				if (!imgCurrentError[i]->IsShown()) imgCurrentError[i]->Show();

				//Cannot read current
				txtCurrentZMK[i]->SetLabel(wxString("-"));

				//Current errors
				bCurrentOK = false;
			}
			else if (bInitTimeout[i])
			{
				if (imgModbusError[i]->IsShown()) imgModbusError[i]->Hide();
				if (imgCurrentOK[i]->IsShown()) imgCurrentOK[i]->Hide();
				if (!imgCurrentError[i]->IsShown()) imgCurrentError[i]->Show();

				//Update current - no information regarding polarities in error mode
				txtCurrentZMK[i]->SetLabel(wxString::Format(wxT("%.2fA"), fCurrent[i]));

				//Current errors
				bCurrentOK = false;
			}
			else //No Errors, display Current OK
			{
				if (imgModbusError[i]->IsShown()) imgModbusError[i]->Hide();
				if (imgCurrentError[i]->IsShown()) imgCurrentError[i]->Hide();
				if (!imgCurrentOK[i]->IsShown()) imgCurrentOK[i]->Show();

				//Update current - no information regarding polarities in error mode
				txtCurrentZMK[i]->SetLabel(wxString::Format(wxT("%.2fA"), fCurrent[i]));
			}
		}
	}

	if (bCurrentOK)
	{
		txtTitleMain->SetLabel(_T("Gotowość Zasilaczy"));
		txtTitleMain->SetForegroundColour(wxColour(34, 188, 34));
	}
	else
	{
		txtTitleMain->SetLabel(_T("Błąd w Inicjalizacji Zasilaczy"));
		txtTitleMain->SetForegroundColour(wxColour(207, 31, 37));
	}

	//Refresh and update frame
	this->Refresh();
	this->Update();
}

void cMainError::OnRefreshDisplay(wxTimerEvent& evt)
{
	RefreshDisplay();
}

/* Frame closed */
void cMainError::OnClose(wxCloseEvent&)
{
	TerminateApp();
}

/* Frame closed from menu */
void cMainError::OnMenuClickedExit(wxCommandEvent& evt)
{
	TerminateApp();
}

void cMainError::TerminateApp()
{
	/******************************************* Stop Refresh  *********************************************/

	tRefreshTimer->Stop();

	/******************************************* Destroy Frame *********************************************/

	Destroy();
}
