#include "cMain.h"

void cMain::OnRemoteSetTract(wxCommandEvent& evt)
{
	{
		wxCriticalSectionLocker event_lock(event_guard);

		//Get data from FIFO buffer
		eRemoteNewTract = eRemoteNewTractFIFO[0];

		//Shift FIFO buffer
		for (uint8_t i = 1; i <= newTractNum; i++) eRemoteNewTractFIFO[i - 1] = eRemoteNewTractFIFO[i];
		eRemoteNewTractFIFO[newTractNum] = None;
		newTractNum--;
	}

	if (!threadSetTract) //if thread not already active
	{
		if (eRemoteNewTract != eActiveTract)
		{
			//Terminate any active set-current threads
			for (uint8_t i = 0; i < NUM_POWER_UNITS; i++)
			{
				if (threadSetCurrent[i] != NULL)
				{
					threadSetCurrent[i]->Delete();
					while (threadSetCurrent[i] != NULL) wxThread::This()->Sleep(1);
				}

				if (mbMaster->mbThreadWritePSU[i] != NULL)
				{
					mbMaster->mbThreadWritePSU[i]->Delete();
					while (mbMaster->mbThreadWritePSU[i] != NULL) wxThread::This()->Sleep(1);
				}
			}

			//Start new thread that handles tract re-configuration
			threadSetTract = new cThreadSetTract(this, mbMaster, eRemoteNewTract);

			if (threadSetTract->Run() != wxTHREAD_NO_ERROR)
			{
				wxLogError("Can't create set-current thread!");
				delete threadSetTract;
				threadSetTract = NULL;
			}
		}
	}

	evt.Skip();
}

void cMain::OnRemoteSetCurrent(wxCommandEvent& evt)
{
	uint8_t unitID;
	float fNewCurrent;

	{
		wxCriticalSectionLocker event_lock(event_guard);

		//Get data from FIFO buffer
		fNewCurrent = fRemoteCurrentFIFO[0];
		unitID = fRemoteUnitFIFO[0];

		//Shift FIFO buffer
		for (uint8_t i = 1; i <= newCurrentNum; i++)
		{
			fRemoteUnitFIFO[i - 1] = fRemoteUnitFIFO[i];
			fRemoteCurrentFIFO[i - 1] = fRemoteCurrentFIFO[i];
		}

		fRemoteUnitFIFO[newCurrentNum] = 0;
		fRemoteCurrentFIFO[newCurrentNum] = 0;
		newCurrentNum--;
	}

	if (!mbMaster->mbThreadWritePSU[unitID - 1])
	{
		//Check current limits
		if (fNewCurrent < (MAX_CURRENT * (-1))) fNewCurrent = MAX_CURRENT * (-1);
		else if (fNewCurrent > MAX_CURRENT) fNewCurrent = MAX_CURRENT;
		txtCtrlCurrentZMK[unitID - 1]->SetValue(wxString::Format(wxT("%.2f"), fNewCurrent));

		//Start new set-current thread
		threadSetCurrent[unitID - 1] = new cThreadSetCurrent(this, mbMaster, unitID, fNewCurrent);

		if (threadSetCurrent[unitID - 1]->Run() != wxTHREAD_NO_ERROR)
		{
			wxLogError("Can't create set-current thread!");
			delete threadSetCurrent[unitID - 1];
			threadSetCurrent[unitID - 1] = NULL;
		}
	}

	evt.Skip();
}
