#include "cThreadTCP.h"

DEFINE_EVENT_TYPE(wxEVT_REMOTE_SET_TRACT)
DEFINE_EVENT_TYPE(wxEVT_REMOTE_SET_CURRENT)

/**************************************** Modbus TCP/IP Thread **********************************************/

cThreadTCP::cThreadTCP(cModbus* mbMaster) : wxThread(wxTHREAD_DETACHED)
{
	this->mbMaster = mbMaster;
	tcpQuery = (uint8_t*)malloc(MODBUS_TCP_MAX_ADU_LENGTH);
    rc = -1;
    rp = -1;
    ct = -1;
    eActiveTract = None;
	unitID = 0;
	regNum = 0;
	regStart = 0;
    regHoldingTemp = 0;
	cCurrent[2] = '.';
	mkNum = 0;
    bTractSet = false;
    bCurrentSet = false;

	//Create new TCP/IP configuration
    ctxTCP = modbus_new_tcp_pi(NULL, "1502");
    modbus_set_indication_timeout(ctxTCP, 0, MB_INDICATION_TIMEOUT_US);
    RestartSocketTCP(ctxTCP);
}

cThreadTCP::~cThreadTCP()
{
	mbMaster->mbThreadTCP = NULL;
	if (socket != -1) close(socket);
    free(tcpQuery);
}

wxThread::ExitCode cThreadTCP::Entry()
{
	while (!TestDestroy())
	{
		if (ct == -1) //connection NOK
		{
			ct = modbus_tcp_pi_accept(ctxTCP, &socket); //wait for new connections (NON-BLOCKING)
			wxThread::Sleep(DELAY_CONNECTION_TCP);
		}
		else //connection OK
		{
			rc = modbus_receive(ctxTCP, tcpQuery); //wait for new indication (BLOCKING, WITH TIMEOUT)

			if (rc != -1) //process client request and send reply
			{
				{
					wxCriticalSectionLocker remote_lock(mbMaster->remote_guard);
					rp = modbus_reply(ctxTCP, tcpQuery, rc, mbMaster->mb_mapping); //send reply
				}

				if (rp != -1) //no reply errors
				{
					if (mbMaster->pMain != NULL)
					{
						if (mbMaster->pMain->eUserMode == Remote) //accept commands only in REMOTE mode
						{
							{
								wxCriticalSectionLocker remote_lock(mbMaster->remote_guard);

								//Check for new commands
								if (mbMaster->mb_mapping->last_indication.function == MODBUS_FC_WRITE_SINGLE_REGISTER ||
									mbMaster->mb_mapping->last_indication.function == MODBUS_FC_WRITE_MULTIPLE_REGISTERS)
								{
									regStart = mbMaster->mb_mapping->last_indication.start_address - mbMaster->mb_mapping->start_registers;
									regNum = mbMaster->mb_mapping->last_indication.numreg;

									//New current settings
									for (int32_t regID = regStart; regID < (regStart + regNum); regID++)
									{
										if (regID >= 0 && regID < NUM_POWER_UNITS)
										{
											//Ensure set-tract thread is not active
											if (!mbMaster->pMain->threadSetTract)
											{
												{
													//Get active tract from Main
													wxCriticalSectionLocker main_lock(mbMaster->pMain->main_guard);
													eActiveTract = mbMaster->pMain->eActiveTract;
												}

												//Get Unit ID
												unitID = regID + 1;

												//Check if PSU is on active tract
												for (uint8_t j = 0; j < TRACTS[eActiveTract].size(); j++)
												{
													mkNum = TRACTS[eActiveTract][j];
													zmkNum[0] = MK_ZMK[mkNum - 1][0];
													zmkNum[1] = MK_ZMK[mkNum - 1][1];

													if (zmkNum[0] == unitID || zmkNum[1] == unitID)
													{
														//Signal thread to trigger Main event
														bCurrentSet = true;
														break;
													}
												}

												//Current set on active unit
												if (bCurrentSet)
												{
													//Copy holding register
													regHoldingTemp = mbMaster->mb_mapping->tab_registers[regID];

													//Convert BCD current to floating point (cCurrent[2] = '.' by default)
													if (regHoldingTemp >> 15) cCurrent[0] = '-';
													else cCurrent[0] = '0';

													cCurrent[1] = ((regHoldingTemp >> 8) & 0xF) + '0';
													cCurrent[3] = ((regHoldingTemp >> 4) & 0xF) + '0';
													cCurrent[4] = (regHoldingTemp & 0xF) + '0';
													cCurrent[5] = '\n';

													{
														wxCriticalSectionLocker event_lock(mbMaster->pMain->event_guard);

														//Add data to FIFO buffers
														mbMaster->pMain->fRemoteCurrentFIFO[mbMaster->pMain->newCurrentNum] = atof(cCurrent);
														mbMaster->pMain->fRemoteUnitFIFO[mbMaster->pMain->newCurrentNum] = unitID;
														if (mbMaster->pMain->newCurrentNum != MAX_REMOTE_EVENTS) mbMaster->pMain->newCurrentNum++;
													}
												}
											}
										}
										else if (regID == NUM_POWER_UNITS) //New Tract Setting
										{
											if (mbMaster->pMain != NULL)
											{
												//Copy holding register
												regHoldingTemp = mbMaster->mb_mapping->tab_registers[regID];

												//Store new tract setting in Main
												if (regHoldingTemp <= NUM_TRACTS)
												{
													bTractSet = true;

													{
														wxCriticalSectionLocker event_lock(mbMaster->pMain->event_guard);

														//Add data to FIFO buffer
														mbMaster->pMain->eRemoteNewTractFIFO[mbMaster->pMain->newTractNum] = (eTract) regHoldingTemp;
														if (mbMaster->pMain->newTractNum != MAX_REMOTE_EVENTS) mbMaster->pMain->newTractNum++;
													}
												}
											}
										}

										//Trigger Main event
										if (bCurrentSet) //new current
										{
											if (mbMaster->pMain != NULL)
											{
												//Trigger event in Main
												wxCommandEvent* evt = new wxCommandEvent(wxEVT_REMOTE_SET_CURRENT, wxID_ANY);
												((wxEvtHandler*) mbMaster->pMain)->QueueEvent(evt);
											}

											bCurrentSet = false;
										}
										else if (bTractSet) //new tract
										{
											if (mbMaster->pMain != NULL)
											{
												//Trigger event in Main
												wxCommandEvent* evt = new wxCommandEvent(wxEVT_REMOTE_SET_TRACT, wxID_ANY);
												((wxEvtHandler*) mbMaster->pMain)->QueueEvent(evt);
											}

											bTractSet = false;
										}
									}
								}
							}
						}
					}
				}
				else if (errno != ENOPROTOOPT) //REPLY ERROR
				{
					RestartSocketTCP(ctxTCP);
				}
			}
			else if (errno != EMBBADDATA && errno != ETIMEDOUT) //RECEIVE ERROR
			{
				RestartSocketTCP(ctxTCP); //connection closed by client or error
			}
		}
	}

	return (wxThread::ExitCode) 0;
}

void cThreadTCP::RestartSocketTCP(modbus_t* ctxTCP)
{
	ct = -1;
	if (socket != -1) close(socket);
    socket = modbus_tcp_pi_listen(ctxTCP, 1); //create new socket
    fcntl(socket, F_SETFL, fcntl(socket, F_GETFL, 0) | O_NONBLOCK); //non-blocking
}
