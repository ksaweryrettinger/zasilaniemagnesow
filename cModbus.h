#pragma once

#include "ZMKConstants.h"
#include "cModbusRTU.h"
#include "cThreadTCP.h"
#include "cEntry.h"
#include "cMain.h"
#include "cMainError.h"
#include "cThreadSetTract.h"
#include "cThreadSetCurrent.h"

class cEntry;
class cMain;
class cMainError;
class cThreadTCP;
class cThreadReadPSU;
class cThreadWritePSU;
class cThreadSetPolarity;
class cThreadSetActiveTract;
class cThreadSetCurrent;
class cThreadSetTract;
class cTimerHighRes;

/* Modbus Gateway class */
class cModbus
{
	public:
		cModbus(cEntry*, cMain*, cMainError*);
		void CloseModbus(void);
		void StartTCP(void);
		void SetPowerUnitCurrent(uint8_t, float);
		void SetPowerUnitPolarity(uint8_t, bool);
		void SetActiveTract(eTract);

	public: //class pointers and flags
		cMain* pMain;
		bool bStartMain;
		bool bStartMainError;

	public: //modbus
		int32_t mbAddress;
		wxString mbPort;
		modbus_t* ctxRTU;
		modbus_mapping_t* mb_mapping;

	public: //critical sections
		wxCriticalSection mb_guard; //prevent multiple Modbus RTU operations
		wxCriticalSection local_guard; //prevent simultaneous local data access
		wxCriticalSection remote_guard; //prevent simultaneous remote data access

	public: //SHARED DATA

		//Errors
		bool bModbusError;
		bool bPSUTimeout[NUM_POWER_UNITS] = { false };
		bool bPSUOverrun[NUM_POWER_UNITS] = { false };
		bool bInitTimeout[NUM_POWER_UNITS] = { false };

		//Readings
		float fCurrent[NUM_POWER_UNITS] = { 0 };
		float fCurrentSetting[NUM_POWER_UNITS] = { 0 };

		//ZMK data
		bool bZMKConnected;
		uint16_t ZNA[2] = { 0 };

	protected: //multithreading
		cThreadTCP* mbThreadTCP;
		cThreadReadPSU* mbThreadReadPSU;
		cThreadWritePSU* mbThreadWritePSU[NUM_POWER_UNITS] = { NULL };
		cThreadSetPolarity* mbThreadSetPolarity[NUM_POWER_UNITS] = { NULL };
		cThreadSetActiveTract* mbThreadSetActiveTract;

	private: //register addressing
		int32_t ZMK_Address[NUM_POWER_UNITS] = { 0 };
		int32_t ZNA_Address[2];
		int32_t ODC_Address;
		int32_t ZAP_Address;

	private:
		friend class cMain;
		friend class cThreadTCP;
		friend class cThreadReadPSU;
		friend class cThreadWritePSU;
		friend class cThreadSetPolarity;
		friend class cThreadSetActiveTract;
		friend class cThreadSetTract;
		friend class cThreadSetCurrent;
};
