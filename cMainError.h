#pragma once

#include "ZMKConstants.h"
#include "cModbus.h"

class cEntry;
class cModbus;
class cConstants;

class cMainError : public wxFrame
{
	public: //constructor
		cMainError(cEntry*);
		void TerminateApp(void);

	private: //event handlers
		void OnMenuClickedExit(wxCommandEvent& evt);
		void OnRefreshDisplay(wxTimerEvent& evt);
		void OnClose(wxCloseEvent& evt);

	private: //other methods
		void RefreshDisplay(void);

	public: //class pointers
		cEntry* pEntry;
		cModbus* mbMaster;
		wxMenu* mHelpMenu;

	private: //menu
		wxMenuBar* mMenuBar;
		wxMenu* mCloseMenu;

	private: //UI elements

		//PANELS
		wxPanel* panZMK[NUM_POWER_UNITS] = { NULL };

		//IMAGES
		wxStaticBitmap* imgModbusError[NUM_POWER_UNITS];
		wxStaticBitmap* imgCurrentError[NUM_POWER_UNITS];
		wxStaticBitmap* imgCurrentOK[NUM_POWER_UNITS];

		//FONTS
		wxFont myFont;

		//STATIC TEXTS
		wxStaticText* txtTitleMain;
		wxStaticText* txtTitleZMK[NUM_POWER_UNITS] = { NULL };
		wxStaticText* txtCurrentZMK[NUM_POWER_UNITS] = { NULL };

		wxTimer* tRefreshTimer;

	private: //Shared Data

		//Errors
		bool bModbusError;
		bool bPSUTimeout[NUM_POWER_UNITS] = { false };
		bool bPSUOverrun[NUM_POWER_UNITS] = { false };
		bool bInitTimeout[NUM_POWER_UNITS] = { false };
		bool bCurrentOK;

		//Current data
		float fCurrent[NUM_POWER_UNITS] = { 0 };

		//Friend classes
		friend class cThreadRefreshDisplay;

		wxDECLARE_EVENT_TABLE();
};


