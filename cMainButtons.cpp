#include "cMain.h"

/* Switch Tract Button event */
void cMain::OnButtonSwitchTract(wxCommandEvent& evt)
{
	if (!threadSetTract) //if thread not already active
	{
		//Get button ID
		int32_t btnID = evt.GetId();
		int32_t tractID = btnID - 10001;

		if (tractID != eActiveTract)
		{
			//Check PSU currents
			for (uint8_t i = 0; i < NUM_POWER_UNITS; i++)
			{
				if (fCurrent[i] >= MIN_SAFE_CURRENT || fCurrentSetting[i] >= MIN_SAFE_CURRENT)
				{
					//Display user dialog
					if (wxMessageBox(_T("\nZmiana traktu spowoduje wyzerowanie\nprądu w zasilaczach."),
							_T("Zmiana Traktu"), wxOK | wxCANCEL | wxICON_INFORMATION) != wxOK) return;
					else break;
				}
			}

			//Zero current setting displays
			for (uint8_t i = 0; i < TRACTS[eActiveTract].size(); i++)
			{
				mkNum = TRACTS[eActiveTract][i];

				//Get ZMK
				zmkNum[0] = MK_ZMK[mkNum - 1][0];
				zmkNum[1] = MK_ZMK[mkNum - 1][1];

				for (uint8_t j = 0; j < MK_ZMK_NUM; j++) txtCtrlCurrentZMK[zmkNum[j] - 1]->SetValue(_T("0.00"));
			}

			//Terminate any active set-current threads
			for (uint8_t i = 0; i < NUM_POWER_UNITS; i++)
			{
				if (threadSetCurrent[i] != NULL)
				{
					threadSetCurrent[i]->Delete();
					while (threadSetCurrent[i] != NULL) wxThread::This()->Sleep(1);
				}

				if (mbMaster->mbThreadWritePSU[i] != NULL)
				{
					mbMaster->mbThreadWritePSU[i]->Delete();
					while (mbMaster->mbThreadWritePSU[i] != NULL) wxThread::This()->Sleep(1);
				}
			}

			//Start new thread that handles tract re-configuration
			threadSetTract = new cThreadSetTract(this, mbMaster, tractID);

		    if (threadSetTract->Run() != wxTHREAD_NO_ERROR)
		    {
		        wxLogError("Can't create set-current thread!");
		        delete threadSetTract;
		        threadSetTract = NULL;
		    }
		}
	}

    evt.Skip();
}

/* Set Current Button event */
void cMain::OnButtonSetCurrent(wxCommandEvent& evt)
{
	//Get button ID
	int32_t btnID = evt.GetId();
	int32_t unitID = btnID - 20000;

	if (!mbMaster->mbThreadWritePSU[unitID - 1])
	{
		float fNewCurrent = 0;

		//Disable button during current change operation
		if (btnCurrentZMK[unitID - 1]->IsEnabled()) btnCurrentZMK[unitID - 1]->Disable();

		//Read current setting
		wxString strCurrent = txtCtrlCurrentZMK[unitID - 1]->GetValue();
		fNewCurrent = wxAtof(strCurrent);

		//Check current limits
		if (fNewCurrent < (MAX_CURRENT * (-1))) fNewCurrent = MAX_CURRENT * (-1);
		else if (fNewCurrent > MAX_CURRENT) fNewCurrent = MAX_CURRENT;
		txtCtrlCurrentZMK[unitID - 1]->SetValue(wxString::Format(wxT("%.2f"), fNewCurrent));

		//Start new set-current thread
		threadSetCurrent[unitID - 1] = new cThreadSetCurrent(this, mbMaster, unitID, fNewCurrent);

		if (threadSetCurrent[unitID - 1]->Run() != wxTHREAD_NO_ERROR)
		{
			wxLogError("Can't create set-current thread!");
			delete threadSetCurrent[unitID - 1];
			threadSetCurrent[unitID - 1] = NULL;
		}
	}

    evt.Skip();
}
