#include "cEntry.h"

wxIMPLEMENT_APP(cEntry);

cEntry::cEntry()
{
	//Initialize variables and pointers
    pMain = nullptr;
    pMainError = nullptr;
	mbMaster = nullptr;
    wxChecker = nullptr;
    dlgUpdate = 0;
    mbAddress = 0;
	ODC_Address = 0;
	ZAP_Address = 0;
    bConfigError = false;
    TimerInit = new cTimerHighRes();
}

bool cEntry::OnInit()
{
    wxApp::SetAppName(wxString(_T("Zasilanie Magnesów Korekcyjnych")));

    {
        wxLogNull logNo; //suppress information message
        wxChecker = new wxSingleInstanceChecker(_T(".zasilaniemagnesow_temp"));
    }

    //Check if process already exists
    if (wxChecker->IsAnotherRunning())
    {
		if (wxMessageBox(_T("\nProces już istnieje.\nZakończyć poprzedni proces?"),
				_T("Zasilanie Magnesów Korekcyjnych"), wxOK | wxCANCEL | wxICON_INFORMATION) == wxOK)
		{
			//Get PID of previous instance
	        string command = "kill $(pidof -o " + to_string(::getpid()) + " ZasilanieMagnesow)";
	        int32_t rtn = system(command.c_str());

	        //Delete old single instance checker
	        command = "rm " + wxString::FromUTF8(getenv("HOME")).ToStdString() + "/.zasilaniemagnesow_temp";
	        rtn = system(command.c_str());
	        rtn = rtn + 1; //to prevent compiler warnings

	        //Create new single instance checker
	        wxLogNull logNo; //suppress information message
			delete wxChecker;
	        wxChecker = new wxSingleInstanceChecker(_T(".zasilaniemagnesow_temp"));
		}
		else
		{
			delete wxChecker;
			wxChecker = nullptr;
			return false;
		}
    }

	//Open configuration file
	filename = wxString::FromUTF8(getenv("HOME")) + "/eclipse-workspace/ZasilanieMagnesow/multizmk.cnf";
	tConfigFile.Open(filename);

	string s;
	vector<string> v;

	while (!tConfigFile.Eof())
	{
		strConfig = tConfigFile.GetNextLine();

		if ((strConfig[0] != '#') && !(strConfig.IsEmpty()))
		{
			s = strConfig.ToStdString();
			boost::trim_if(s, boost::is_any_of("\t "));
			boost::split(v, s, boost::is_any_of(" \t"), boost::token_compress_on);

			if (v[0] == "MBS") //Modbus Slave Address
			{
				if (v.size() >= 2) mbAddress = wxAtoi(v[1]);
				else
				{
					bConfigError = true;
					break;
				}
			}
			else if (v[0] == "TTY") //Linux Serial Port
			{
				if (v.size() >= 2) mbPort = v[1];
				else
				{
					bConfigError = true;
					break;
				}
			}
			else if (v[0] == "ZNA") //ZNA Register Addressing
			{
				if (v.size() >= 3)
				{
					ZNA_Address[wxAtoi(v[1])] = strtol(("0x" + v[2]).c_str(), NULL, 16);
				}
				else
				{
					bConfigError = true;
					break;
				}
			}
			else if (v[0] == "ZMK") //ZMK Register Addressing
			{
				if (v.size() >= 3) ZMK_Address[wxAtoi(v[1])] = strtol(("0x" + v[2]).c_str(), NULL, 16);
				else
				{
					bConfigError = true;
					break;
				}
			}
			else if (v[0] == "ODC") //ODC Register Address
			{
				if (v.size() >= 2) ODC_Address = strtol(("0x" + v[1]).c_str(), NULL, 16);
				else
				{
					bConfigError = true;
					break;
				}
			}
			else if (v[0] == "ZAP") //ZAP Register Address
			{
				if (v.size() >= 2) ZAP_Address = strtol(("0x" + v[1]).c_str(), NULL, 16);
				else
				{
					bConfigError = true;
					break;
				}
			}
		}
	}

	if (bConfigError)
	{
		 wxLogError(_T("\n       Błąd w pliku konfiguracyjnym!      "));
		 return false;
	}

    //Create Main window
    pMain = new cMain(this);
	pMain->SetIcon(wxICON(icon));

	//Create Main Error window
    pMainError = new cMainError(this);
	pMainError->SetIcon(wxICON(icon));

	//Start Modbus master
	mbMaster = new cModbus(this, pMain, pMainError);

	//Create user dialog
	wxProgressDialog pProgressDlg(_T("Rozpoczęcie Pracy"), _T("Inicjalizowanie zasilaczy..."), 100, pMain, wxPD_SMOOTH | wxPD_APP_MODAL | wxPD_AUTO_HIDE);

	//Reset start-up timer
	TimerInit->reset();

	//Update progress dialog
	while (!mbMaster->bStartMain && !mbMaster->bStartMainError)
	{
		wxThread::Sleep(200);
		dlgUpdate = (int32_t) ((TimerInit->elapsed() / ((double) INIT_PSU_TIMEOUT)) * 100);
		if (dlgUpdate <= 100) pProgressDlg.Update(dlgUpdate);
		else pProgressDlg.Update(100);
	}

	//Final process dialog udpate
	if (dlgUpdate <= 100)
	{
		wxThread::Sleep(200);
		pProgressDlg.Update(100);
	}

	if (mbMaster->bStartMainError)
	{
		//Terminate Main window
		pMain->TerminateApp(true);

		//Show error window after delay
		wxThread::Sleep(1000);
		pMainError->Show();
	}
	else
	{
		//Terminate error window
		pMainError->TerminateApp();

		//Show main window after delay
		wxThread::Sleep(1000);
		pMain->Show();
	}

	return true;
}

int cEntry::OnExit()
{
    //Close Modbus gateway and free resources
    mbMaster->CloseModbus();

    //Free pointers
    delete mbMaster;
    delete wxChecker;
    mbMaster = nullptr;
    wxChecker = nullptr;

    return 0;
}
