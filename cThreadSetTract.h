#pragma once

#include "ZMKConstants.h"
#include "cMain.h"
#include "cThreadSetCurrent.h"

class cMain;
class cModbus;
class cThreadSetCurrent;
class cTimerHighRes;

/* System clock thread */
class cThreadSetTract : public wxThread
{
	public:
		cThreadSetTract(cMain*, cModbus*, uint8_t);
		~cThreadSetTract();

	protected:
		virtual ExitCode Entry();

	private:
		void RefreshModbusData(void);
		void RefreshMainData(void);
		bool TestErrors(void);

	private:
		cMain* pMain;
		cModbus* mbMaster;

	private: //MODBUS DATA

		//Errors
		bool bModbusError;
		bool bPSUTimeout[NUM_POWER_UNITS] = { false };
		bool bPSUOverrun[NUM_POWER_UNITS] = { false };

		//Readings
		float fCurrent[NUM_POWER_UNITS] = { 0 };
		float fCurrentSetting[NUM_POWER_UNITS] = { 0 };

	private: //OTHER DATA
		uint8_t mkNum;
		uint8_t zmkNum[2] = { 0 };
		eTract eNewTract;
		eTract eActiveTract;
		bool bTimeout;
		bool bCurrentOK;
		cTimerHighRes* TimerSwitchTract;
};
