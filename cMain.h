#pragma once

#include "ZMKConstants.h"
#include "cModbus.h"
#include "cThreadSetCurrent.h"
#include "cThreadSetTract.h"
#include "cInformation.h"
#include "cPowerCheck.h"
#include "cThreadTCP.h"

class cEntry;
class cModbus;
class cInformation;
class cPowerCheck;
class cTimerHighRes;
class cThreadSetTract;
class cThreadSetCurrent;
class cThreadRefreshDisplay;
class cThreadTCP;

class cMain : public wxFrame
{
	public: //constructor
		cMain(cEntry*);
		void TerminateApp(bool);

	private: //event handlers
		void OnMenuClickedInformation(wxCommandEvent& evt);
		void OnMenuClickedMode(wxCommandEvent& evt);
		void OnMenuClickedExit(wxCommandEvent& evt);
		void OnRefreshDisplay(wxTimerEvent& evt);
		void OnButtonSwitchTract(wxCommandEvent& evt);
		void OnButtonSetCurrent(wxCommandEvent& evt);
		void OnRemoteSetTract(wxCommandEvent& evt);
		void OnRemoteSetCurrent(wxCommandEvent& evt);
		void OnEraseBackground(wxEraseEvent& event);
		void OnClose(wxCloseEvent& evt);

	private: //other methods
		void RefreshDisplay(void);

	public: //class pointers
		cEntry* pEntry;
		cModbus* mbMaster;
		cInformation* pInformation;
		cPowerCheck* pPowerCheck;
		wxMenu* mHelpMenu;
		bool informationWindowIsOpen;

	public: //other public variables
		wxCriticalSection main_guard; //main data guard
		wxCriticalSection event_guard; //TCP/IP event data guard
		eTract eActiveTract;
		eTract eNewTract;
		eMode eUserMode;
		bool bSetTractError;
		bool bSetCurrentError[NUM_POWER_UNITS] = { false };
		bool bTractSwitching;

	public: //remote events
		eTract eRemoteNewTract;
		uint8_t fRemoteUnitFIFO[MAX_REMOTE_EVENTS] = { 0 };
		float fRemoteCurrentFIFO[MAX_REMOTE_EVENTS] = { 0 };
		eTract eRemoteNewTractFIFO[MAX_REMOTE_EVENTS] = { None };
		uint8_t newTractNum;
		uint8_t newCurrentNum;

	private: //menu
		wxMenuBar* mMenuBar;
		wxMenu* mCloseMenu;
		wxMenu* mModeMenu;

	private: //UI elements

		//PANELS
		wxPanel* panTractSelect;
		wxPanel* panTractDisplay;
		wxPanel* panControls;
		wxPanel* panZMKConnected;
		wxPanel* panMK[6] = { NULL };

		//IMAGES
		wxStaticBitmap* imgTracts[NUM_TRACTS + 1];
		wxStaticBitmap* imgTractsMBError;
		wxStaticBitmap* imgZMKConnectedBox;
		wxStaticBitmap* imgZMKConnected;
		wxStaticBitmap* imgZMKDisconnected;
		wxStaticBitmap* imgModbusError[NUM_POWER_UNITS];
		wxStaticBitmap* imgCurrentError[NUM_POWER_UNITS];
		wxStaticBitmap* imgCurrentSet[NUM_POWER_UNITS];
		wxStaticBitmap* imgCurrentOK[NUM_POWER_UNITS];

		//FONTS
		wxFont myFont;

		//STATIC TEXTS
		wxStaticText* txtModbusError;
		wxStaticText* txtSetTractError;
		wxStaticText* txtTitleTractSelect;
		wxStaticText* txtTitleControls;
		wxStaticText* txtTitleZMKConnected;
		wxStaticText* txtZMKConnectedMBError;
		wxStaticText* txtCurrentZMK[NUM_POWER_UNITS] = { NULL };

		//CONTROLS
		wxTextCtrl* txtCtrlCurrentZMK[NUM_POWER_UNITS] = { NULL };

		//BUTTONS
		wxButton* btnTractSelect[NUM_TRACTS + 1] = { NULL };
		wxButton* btnCurrentZMK[NUM_POWER_UNITS] = { NULL };

		//THREADS
		cThreadSetTract* threadSetTract = NULL;
		cThreadSetCurrent* threadSetCurrent[NUM_POWER_UNITS] = { NULL };

	private: //Shared Data

		//Errors
		bool bModbusError;
		bool bPSUTimeout[NUM_POWER_UNITS] = { false };
		bool bPSUOverrun[NUM_POWER_UNITS] = { false };

		//Current data
		float fCurrent[NUM_POWER_UNITS] = { 0 };
		float fCurrentSetting[NUM_POWER_UNITS] = { 0 };

		//ZMK data
		bool bZMKConnected;
		uint16_t ZNA[2] = { 0 };

	private: //other data

		wxTimer* tRefreshTimer;
		cTimerHighRes* TimerCloseApp;
		uint8_t mkNum;
		uint8_t zmkNum[2] = { 0 };
		uint8_t zmkIndex;
		bool bCurrentOK;

		//Friend classes
		friend class cThreadRefreshDisplay;
		friend class cThreadSetTract;
		friend class cThreadSetCurrent;
		friend class cThreadTCP;

		wxDECLARE_EVENT_TABLE();
};


