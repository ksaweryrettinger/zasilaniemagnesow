#pragma once

//EXTERNAL LIBRARIES
#include <boost/algorithm/string.hpp>
#include <regex.h>
#include <string>
#include <locale>
#include <vector>
#include <modbus.h>
#include <unistd.h>
#include <algorithm>
#include <chrono>
#include <ifaddrs.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <limits.h>
#include <map>
#include <wx/wx.h>
#include <wx/log.h>
#include <wx/textfile.h>
#include <wx/snglinst.h>
#include <wx/progdlg.h>
#include <wx/statline.h>

//NAMESPACES
using namespace std;

//DEBUG MODES
const bool CHECK_INIT_ERRORS	  = true; //default: true
const bool CHECK_TRACT_ERRORS 	  = true; //default: true
const bool CHECK_CURRENT_ERRORS   = true; //default: true
const bool STORE_CURRENT_READINGS = true; //default: true (false = display current settings as readings)

//APPLICATION MODES AND TRACTS
typedef enum { Local = 0, Remote = 1} eMode;
typedef enum { None = 0, A = 1, B = 2, C1 = 3, C2 = 4, C3 = 5, D = 6 } eTract;

//TRACT NAMES
const wxString TRACT_NAMES[] = {"Brak", 'A', 'B', "C-1", "C-2", "C-3", 'D'};

//TRACT - MK MAPPING
const vector<uint8_t> TRACTS[7] = {{1, 2},					//None
								   {1, 2, 3, 4}, 			//Tract A
								   {1, 2, 6, 7}, 			//Tract B
								   {1, 2, 8, 9, 10} ,		//Tract C1
								   {1, 2, 8, 9, 12, 21},	//Tract C2 (21 = MKX2)
								   {1, 2, 8, 9, 13, 14},    //Tract C3
								   {1, 2, 16, 17, 18, 19}}; //Tract D

//MK - ZMK MAPPING
const uint8_t MK_ZMK[21][2] = {{1, 2},    //MK1
						       {3, 4},    //MK2
							   {5, 6},    //MK3
							   {7, 8},    //MK4
							   {9, 10},   //MK5
							   {5, 6},    //MK6
							   {7, 8},    //MK7
							   {5, 6},    //MK8
							   {7, 8},    //MK9
							   {9, 10},   //MK10
							   {11, 12},  //MK11
							   {9, 10},   //MK12
							   {9, 10},   //MK13
							   {11, 12},  //MK14
							   {11, 12},  //MK15
							   {5, 6},    //MK16
							   {7, 8},    //MK17
							   {9, 10},   //MK18
							   {11, 12},  //MK19
							   {9, 10},   //MKX1 (20)
							   {11, 12}}; //MKX2 (21)

//MK - ZNA BIT MAPPING
const int8_t MK_ZNA[21] = {-1,    //MK1 (dummy)
						   -1,    //MK2 (dummy)
							0,    //MK3
							1,    //MK4
							2,    //MK5
							3,    //MK6
							4,    //MK7
				    		6,    //MK8
							7,    //MK9
							8,    //MK10
							9,    //MK11
							10,   //MK12
							12,   //MK13
							13,   //MK14
							14,   //MK15
							15,   //MK16
							0,    //MK17 (ZNA 1)
							1,    //MK18 (ZNA 1)
							2,    //MK19 (ZNA 1)
							5,    //MKX1 (20, ZNA 0)
							11};  //MKX2 (21, ZNA 0)

//ZMK - ZAP BIT MAPPING (B0-B4)
const uint16_t ZMK_ZAP[12] = {0b00110,  //ZMK1
							 0b01110,  //ZMK2
							 0b10110,  //ZMK3
							 0b11110,  //ZMK4
							 0b00101,  //ZMK5
							 0b01101,  //ZMK6
							 0b10101,  //ZMK7
							 0b11101,  //ZMK8
							 0b00011,  //ZMK9
							 0b01011,  //ZMK10
							 0b10011,  //ZMK11
							 0b11011}; //ZMK12

//MODBUS TCP/IP MAPPING
const uint8_t MB_INPUTREG_CUR_START    = 1;
const uint8_t MB_INPUTREG_CUR_END      = 12;
const uint8_t MB_INPUTREG_SETCUR_START = 13;
const uint8_t MB_INPUTREG_SETCUR_END   = 24;
const uint8_t MB_INPUTREG_ZMK_STATUS   = 25;
const uint8_t MB_INPUTREG_POLARITY     = 26;
const uint8_t MB_INPUTREG_ACTIVE_TRACT = 27;
const uint8_t MB_INPUTREG_RTU_ERROR    = 28;
const uint8_t MB_INPUTREG_PSU_TIMEOUT  = 29;
const uint8_t MB_INPUTREG_PSU_OVERRUN  = 30;
const uint8_t MB_INPUTREG_USERMODE     = 31;

//OTHER
const int32_t MAX_REMOTE_EVENTS = 100;
const int32_t NUM_POWER_UNITS = 12;
const int32_t NUM_MAGNETS = 17;
const int32_t MK_ZMK_NUM = 2;
const int32_t NUM_TRACTS = 6;
const float ADC_1A_WRITE = 492.68f;
const float ADC_1A_READ = 256.0f;
const float MIN_SAFE_CURRENT = 0.1f;
const float MAX_CURRENT = 8.0f;
const float H_AMP = 0.5f;

//MODBUS CONFIGURATION
const int32_t MB_BAUD = 115200; //Modbus RTU Baud Rate
const int32_t MB_RESPONSE_TIMEOUT_SEC = 0; //Modbus RTU response timeout (seconds)
const int32_t MB_RESPONSE_TIMEOUT_US = 75000; //Modbus RTU response timeout (us)
const int32_t MB_INDICATION_TIMEOUT_US = 150000; //Modbus TCP/IP Indication timeout (us)

//THREAD DELAYS
const int32_t DELAY_REFRESH_DISPLAY   = 211;  //ms, main display refresh
const int32_t DELAY_CURRENT_STEP	  = 149;  //ms, delay between current steps
const int32_t DELAY_TRACT_SWITCH 	  = 557;  //ms, cThreadSetTract data refresh
const int32_t DELAY_SET_CURRENT       = 997;  //ms, cThreadSetCurrent data refresh
const int32_t DELAY_CURRENT_READY 	  = 60;   //ms, time before current reading is ready
const int32_t DELAY_CURRENT_REPEAT 	  = 5;    //ms, time between current read attempts
const int32_t DELAY_CURRENT_NEXTUNIT  = 50;   //ms, time between current read attempts on different PSUs
const int32_t DELAY_CONNECTION_TCP	  = 100;  //ms, delay between subsequent TCP connetion attempts
const int32_t DELAY_CLOSE_APP		  = 1000; //ms, delay before closing app (successful close)

//TIMEOUTS
const int32_t INIT_PSU_TIMEOUT 		  = 6000; //ms
const int32_t SET_CURRENT_TIMEOUT     = 6000; //ms
const int32_t SWITCH_TRACT_TIMEOUT    = 6000; //ms
const int32_t READ_CURRENT_TIMEOUT 	  = 200;  //ms
const int32_t CLOSE_APP_TIMEOUT		  = 6000; //ms
