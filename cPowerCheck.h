#pragma once

#include "ZMKConstants.h"
#include "cModbus.h"

class cModbus;

class cPowerCheck : public wxFrame
{
	public: //constructor
		cPowerCheck(cModbus*);

	private: //event handlers
		void OnRefreshDisplay(wxTimerEvent& evt);
		void RefreshDisplay(void);
		void TerminateApp(void);

	private: //class pointers
		cModbus* mbMaster;
		wxStaticBitmap* imgWarning;
		wxStaticText* txtWarning;
		wxFont myFont;

	private: //Shared Data
		bool bModbusError;
		bool bZMKConnected;

	private: //other
		wxTimer* tRefreshTimer;

		wxDECLARE_EVENT_TABLE();
};
